@extends('layouts.master')
@section('content')
    <section>
        <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
        <div class="aboutus-secktion paddingTB60">
            <div class="container">
                <div class="row py-4">
                    <div class="col-md-8 bg-light">
                        <form method="post" id="customer-signup-form myForm" class="job-manager-form"
                            action="{{ route('submitBooking', app()->getLocale()) }}">
                            @csrf
                            <div class="bg-dark text-light px-3 py-2">BOOK YOUR SERVICE - {{ $service->service }} </div>
                            <div class="row" style="margin-top:5%;">
                                <input type="hidden" class="form-control customer-input" id="service" required=""
                                    readonly="">
                                <input type="hidden" class="form-control customer-input" id="service_id" name="service_id"
                                    value="{{ $service->id }}">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address<span style="color:red;">*</span></label>
                                        <textarea class="form-control" placeholder=" Enter Your Address" id="address" name="address" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contact Number<span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter Your Name "
                                            id="contact" name="contact" value="{{Auth::user()->customerphonenumber}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Person Required ?<span style="color:red;">*</span></label>
                                        <input type="number" class="form-control" id="work_time" name="work_time"
                                            required="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Service Rate per Person (AED)<span style="color:red;">*</span></label>
                                        <input type="number" class="form-control" id="rate" name="rate" required="" value="{{ $service->rate }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>VAT</label>
                                        <input type="number" class="form-control" id="vat" name="vat" value="{{(int) $service->vat ?? 0 }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Total Amount (AED)</label>
                                        <input type="number" class="form-control" id="rate" name="rate" value="{{ (int)$service->rate + (int)$service->vat   }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comments(if any)</label>
                                        <textarea class="form-control" placeholder=" Enter Your Address" id="comment" name="comment"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input class="btn btn-primary btn-block my-4" type="submit" value="Proceed to Checkout"
                                        name="AppointBtn" id="AppointBtn">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <div class="sf-featured-bx item">
                                <div class="sf-element-bx">
                                    <div class="sf-thum-bx img-effect2">
                                        <a href="#"
                                            style="background:url('https://www.serviceprovider.ae/beta/sp1/public/uploads/istockphoto-1215251260-170667a.jpg');height: 200px;background-size: cover;background-position: 50%;display: block;">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-text">
                                                <div class="sf-address-bx">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="sf-category-tag"><a
                                                href="#">Pest Control &amp;
                                                Disinfection </a></strong>
                                    </div>
                                    <div class="padding-20 bg-white">
                                        <h4 class="sf-title">
                                            <a href="#">1BR Apartment Pest
                                                Control</a>
                                        </h4>
                                        <p>1BR Apartment Pest Control Service</p>
                                    </div>
                                    <div class="btn-group-justified" id="proid-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="sf-featured-bx item">
                                <div class="sf-element-bx">
                                    <div class="sf-thum-bx img-effect2">
                                        <a href="#"
                                            style="background:url('https://www.serviceprovider.ae/beta/sp1/public/uploads/visa-3653492__480 (1).webp');height: 200px;background-size: cover;background-position: 50%;display: block;">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-text">
                                                <div class="sf-address-bx">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="sf-category-tag"><a
                                                href="#">Dubai Visa
                                                Package</a></strong>
                                    </div>
                                    <div class="padding-20 bg-white">
                                        <h4 class="sf-title">
                                            <a href="#">2 Years Residency
                                                Visa</a>
                                        </h4>
                                        <p>Get An Emirates ID For 2 Years</p>
                                    </div>
                                    <div class="btn-group-justified" id="proid-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
