@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px; margin-bottom:10px;  background-color: #111;"></div>
    <div class="aboutus-secktion paddingTB60">
        <div class="container">
            <div class="row py-4">
                <div class="about-author d-flex p-4 bg-light">
                    <div class="col-md-4 bio mr-5">
                      <img src="{{url('')}}/assets/images/person_1.jpg" alt="Image placeholder" class="img-fluid mb-4">
                    </div>
                    <div class="desc">
                      <h3>Who we are and<br>what we do</h3>
                      <p>Serviceprovider.ae is one of the largest service provider portals in the region managed by RHODIUM
                        KHADAMAT PORTAL EST. It is a one-stop solution for all your home, office, health, lifestyle, and on
                        the road needs.... We simplify your everyday living with a huge variety of at-home services across
                        various cities, countries, and regions. All work is conducted with due diligence by our verified,
                        qualified, and carefully chosen service providers in the city. The portal is managed and operated by
                        service industry experts with decades of service industry experience. Let's build the services world
                        together.</p>
                    </div>
                  </div>
      
             
            </div>
            
        </div>
        <section class="ftco-section ftco-intro" style="background-image: url('{{url('')}}/assets/images/bg_3.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h2>Together we will explore new things</h2>
                        <a href="" class="icon-video d-flex align-items-center justify-content-center"><span class="fa fa-play"></span></a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
