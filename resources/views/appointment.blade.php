@extends('layouts.master')
@section('content')
    <section>
        <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
        <div class="aboutus-secktion paddingTB60">
            <div class="container">
                <div class="row py-4">
                    <div class="col-md-8 bg-light">
                        <form method="post" id="customer-signup-form myForm" class="job-manager-form"
                            action="{{ route('booking', app()->getLocale()) }}">
                            @csrf
                            <div class="bg-dark text-light px-3 py-2">Book your Appointment -
                                {{ $service->pservice->service }} </div>
                            <div class="row" style="margin-top:5%;">
                                <input type="hidden" class="form-control customer-input" id="service"
                                    value="{{ $service->pservice->service }}" required="" readonly="">
                                <input type="hidden" class="form-control customer-input" id="service_id" name="service_id"
                                    value="{{ $service->id }}">
                                <input type="hidden" class="form-control customer-input" id="unitname" name="unitname"
                                    value="{{ $service->unit }}">
                                <input type="hidden" class="form-control customer-input" id="cemail" name="cemail"
                                    value="{{ Auth::user()->customeremail }}">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address<span style="color:red;">*</span></label>
                                        <textarea class="form-control" placeholder=" Enter Your Address" id="address" name="address" required=""
                                            style="overflow: hidden;">{{ $appointment->address ?? null }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contact Number<span style="color:red;">*</span></label>
                                        <input type="number" class="form-control" id="phone" name="contact" required=""
                                            value="{{ Auth::user()->customerphonenumber ?? null }}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label>Date<span style="color:red;">*</span></label>
                                        <input type="date" min="2022-06-02" class="form-control srdate" id="adate"
                                            name="adate" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                                    <label>Select time slot<span style="color:red;">*</span></label>
                                    <select class="form-control bs-select-hidden" id="stime" name="time" placeholder=""
                                        required="">
                                        <option value="0" selected="">--please select--</option>
                                        <option value="00:00 to 01:00">00:00 to 01:00</option>
                                        <option value="01:00 to 02:00">01:00 to 02:00</option>
                                        <option value="02:00 to 03:00">02:00 to 03:00</option>
                                        <option value="03:00 to 04:00">03:00 to 04:00</option>
                                        <option value="04:00 to 05:00">04:00 to 05:00</option>
                                        <option value="05:00 to 06:00">05:00 to 06:00</option>
                                        <option value="06:00 to 07:00">06:00 to 07:00</option>
                                        <option value="07:00 to 08:00">07:00 to 08:00</option>
                                        <option value="08:00 to 09:00">08:00 to 09:00</option>
                                        <option value="09:00 to 10:00">09:00 to 10:00</option>
                                        <option value="10:00 to 11:00">10:00 to 11:00</option>
                                        <option value="11:00 to 12:00">11:00 to 12:00</option>
                                        <option value="12:00 to 13:00">12:00 to 13:00</option>
                                        <option value="13:00 to 14:00">13:00 to 14:00</option>
                                        <option value="14:00 to 15:00">14:00 to 15:00</option>
                                        <option value="15:00 to 16:00">15:00 to 16:00</option>
                                        <option value="16:00 to 17:00">16:00 to 17:00</option>
                                        <option value="17:00 to 18:00">17:00 to 18:00</option>
                                        <option value="18:00 to 19:00">18:00 to 19:00</option>
                                        <option value="19:00 to 20:00">19:00 to 20:00</option>
                                        <option value="20:00 to 21:00">20:00 to 21:00</option>
                                        <option value="21:00 to 22:00">21:00 to 22:00</option>
                                        <option value="22:00 to 23:00">22:00 to 23:00</option>
                                        <option value="23:00 to 00:00">23:00 to 00:00</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label> Unit?<span style="color:red;">*</span></label>
                                        <input type="number" class="form-control" id="time" name="work_time" required=""
                                            value="{{ $service->unitp->name }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>How many persons do you need?<span style="color:red;">*</span></label>
                                        <input type="number" class="form-control" id="heroes" name="heroes" required=""
                                            value="0">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Service Rate per (AED)</label>
                                        <input type="number" class="form-control" id="num" name="num"
                                            value="{{ $service->rate }}" required="" readonly="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Total Amount (AED)</label>
                                        <input type="number" class="form-control" id="total" name="total"
                                            value="{{ $service->rate + $service->vat }}" required="" readonly="">
                                        <input type="hidden" class="form-control" id="rate" name="rate"
                                            value="{{ $service->rate + $service->vat }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Do you have any special instructions?</label>
                                        <textarea rows="10" class="form-control" placeholder=" Enter Your instruction" id="instruction" name="instruction"
                                            style="overflow: hidden;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input class="btn btn-primary btn-block my-4" type="submit" value="Proceed to Checkout"
                                        name="AppointBtn" id="AppointBtn">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <div class="sf-featured-bx item">
                                <div class="sf-element-bx">
                                    <div class="sf-thum-bx img-effect2">
                                        <a href="#"
                                            style="background:url('https://www.serviceprovider.ae/beta/sp1/public/uploads/istockphoto-1215251260-170667a.jpg');height: 200px;background-size: cover;background-position: 50%;display: block;">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-text">
                                                <div class="sf-address-bx">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="sf-category-tag"><a
                                                href="#">Pest Control &amp;
                                                Disinfection </a></strong>
                                    </div>
                                    <div class="padding-20 bg-white">
                                        <h4 class="sf-title">
                                            <a href="#">1BR Apartment Pest
                                                Control</a>
                                        </h4>
                                        <p>1BR Apartment Pest Control Service</p>
                                    </div>
                                    <div class="btn-group-justified" id="proid-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="sf-featured-bx item">
                                <div class="sf-element-bx">
                                    <div class="sf-thum-bx img-effect2">
                                        <a href="#"
                                            style="background:url('https://www.serviceprovider.ae/beta/sp1/public/uploads/visa-3653492__480 (1).webp');height: 200px;background-size: cover;background-position: 50%;display: block;">

                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-text">
                                                <div class="sf-address-bx">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="sf-category-tag"><a
                                                href="#">Dubai Visa
                                                Package</a></strong>
                                    </div>
                                    <div class="padding-20 bg-white">
                                        <h4 class="sf-title">
                                            <a href="#">2 Years Residency
                                                Visa</a>
                                        </h4>
                                        <p>Get An Emirates ID For 2 Years</p>
                                    </div>
                                    <div class="btn-group-justified" id="proid-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
