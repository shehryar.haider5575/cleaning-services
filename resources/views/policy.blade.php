@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px;background-color: black"></div>
    <div class="page-content">
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="url('/')">Home</a></li>
                    <li>Privacy Policy</li>
                </ul>
            </div>
        </div>
        <div class="section-content profiles-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Privacy Policy for service provider</h2>
                        <p style="text-align: left;">At serviceprovider.ae, accessible from info@serviceprovider.ae, one of
                            our main priorities is the privacy of our visitors. This Privacy Policy document contains types
                            of information that is collected and recorded by serviceprovider.ae and how we use it.</p>
                        <p>If you have additional questions or require more information about our Privacy Policy, do not
                            hesitate to contact us.</p>
                        <p style="text-align: left;">This Privacy Policy applies only to our online activities and is valid
                            for visitors to our website with regards to the information that they shared and/or collect in
                            serviceprovider.ae. This policy is not applicable to any information collected offline or via
                            channels other than this website. Our Privacy Policy was created with the help of the Privacy
                            Policy Generator.</p>
                        <h4>Consent</h4>
                        <p>By using our website, you hereby consent to our Privacy Policy and agree to its terms.</p>
                        <h4>Information we collect</h4>
                        <p>The personal information that you are asked to provide, and the reasons why you are asked to
                            provide it, will be made clear to you at the point we ask you to provide your personal
                            information.</p>
                        <p>If you contact us directly, we may receive additional information about you such as your name,
                            email address, phone number, the contents of the message and/or attachments you may send us, and
                            any other information you may choose to provide.</p>
                        <p>When you register for an Account, we may ask for your contact information, including items such
                            as name, company name, address, email address, and telephone number.</p>
                        <h4>How we use your information</h4>
                        <h4>We use the information we collect in various ways, including to:</h4>
                        <ul>
                            <li>
                                <p>Provide, operate, and maintain our website</p>
                            </li>
                            <li>
                                <p>Improve, personalize, and expand our website</p>
                            </li>
                            <li>
                                <p>Understand and analyze how you use our website</p>
                            </li>
                            <li>
                                <p>Develop new products, services, features, and functionality</p>
                            </li>
                            <li>
                                <p>Communicate with you, either directly or through one of our partners, including for
                                    customer service, to provide you with updates and other information relating to the
                                    website, and for marketing and promotional purposes</p>
                            </li>
                            <li>
                                <p>Send you emails</p>
                            </li>
                            <li>
                                <p>Find and prevent fraud</p>
                            </li>
                        </ul>
                        <h4>Log Files</h4>
                        <p>serviceprovider.ae follows a standard procedure of using log files. These files log visitors when
                            they visit websites. All hosting companies do this and a part of hosting services’ analytics.
                            The information collected by log files include internet protocol (IP) addresses, browser type,
                            Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the
                            number of clicks. These are not linked to any information that is personally identifiable. The
                            purpose of the information is for analyzing trends, administering the site, tracking users’
                            movement on the website, and gathering demographic information.</p>
                        <h4>Cookies and Web Beacons</h4>
                        <p>Like any other website, serviceprovider.ae uses ‘cookies’. These cookies are used to store
                            information including visitors’ preferences, and the pages on the website that the visitor
                            accessed or visited. The information is used to optimize the users’ experience by customizing
                            our web page content based on visitors’ browser type and/or other information.</p>
                        <p>For more general information on cookies, please read “What Are Cookies”.</p>
                        <h4>Google DoubleClick DART Cookie</h4>
                        <p>Google is one of a third-party vendor on our site. It also uses cookies, known as DART cookies,
                            to serve ads to our site visitors based upon their visit to<strong> <a
                                    href="https://www.serviceprovider.ae/">www.serviceprovider.ae</a></strong> and other
                            sites on the internet. However, visitors may choose to decline the use of DART cookies by
                            visiting the Google ad and content network Privacy Policy at the following URL – <strong><a
                                    href="https://policies.google.com/technologies/ads">https://policies.google.com/technologies/ads</a>.</strong>
                        </p>
                        <h4>Advertising Partners Privacy Policies</h4>
                        <p>You may consult this list to find the Privacy Policy for each of the advertising partners of
                            serviceprovider.ae.</p>
                        <p>Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons
                            that are used in their respective advertisements and links that appear on serviceprovider.ae,
                            which are sent directly to users’ browser. They automatically receive your IP address when this
                            occurs. These technologies are used to measure the effectiveness of their advertising campaigns
                            and/or to personalize the advertising content that you see on websites that you visit.</p>
                        <p>Note that serviceprovider.ae has no access to or control over these cookies that are used by
                            third-party advertisers.</p>
                        <h4>Third Party Privacy Policies</h4>
                        <p>serviceprovider.ae’s Privacy Policy does not apply to other advertisers or websites. Thus, we are
                            advising you to consult the respective Privacy Policies of these third-party ad servers for more
                            detailed information. It may include their practices and instructions about how to opt-out of
                            certain options.</p>
                        <p>You can choose to disable cookies through your individual browser options. To know more detailed
                            information about cookie management with specific web browsers, it can be found at the browsers’
                            respective websites.</p>
                        <h4>CCPA Privacy Rights (Do Not Sell My Personal Information)</h4>
                        <h4>Under the CCPA, among other rights, California consumers have the right to:</h4>
                        <ul>
                            <li>
                                <p>Request that a business that collects a consumer’s personal data disclose the categories
                                    and specific pieces of personal data that a business has collected about consumers.
                                </p>
                            </li>
                            <li>
                                <p>Request that a business delete any personal data about the consumer that a business has
                                    collected.
                                </p>
                            </li>
                            <li>
                                <p>Request that a business that sells a consumer’s personal data, not sell the consumer’s
                                    personal data.
                                </p>
                            </li>
                            <li>
                                <p>If you make a request, we have one month to respond to you. If you would like to exercise
                                    any of these rights, please contact us.</p>
                            </li>
                        </ul>
                        <h4>GDPR Data Protection Rights</h4>
                        <h4>We would like to make sure you are fully aware of all of your data protection rights. Every user
                            is entitled to the following:</h4>
                        <ul>
                            <li>
                                <p>The right to access – You have the right to request copies of your personal data. We may
                                    charge you a small fee for this service.
                                </p>
                            </li>
                            <li>
                                <p>The right to rectification – You have the right to request that we correct any
                                    information you believe is inaccurate. You also have the right to request that we
                                    complete the information you believe is incomplete.
                                </p>
                            </li>
                            <li>
                                <p>The right to erasure – You have the right to request that we erase your personal data,
                                    under certain conditions.
                                </p>
                            </li>
                            <li>
                                <p>The right to restrict processing – You have the right to request that we restrict the
                                    processing of your personal data, under certain conditions.
                                </p>
                            </li>
                            <li>
                                <p>The right to object to processing – You have the right to object to our processing of
                                    your personal data, under certain conditions.
                                </p>
                            </li>
                            <li>
                                <p>The right to data portability – You have the right to request that we transfer the data
                                    that we have collected to another organization, or directly to you, under certain
                                    conditions.
                                </p>
                            </li>
                            <li>
                                <p>If you make a request, we have one month to respond to you. If you would like to exercise
                                    any of these rights, please contact us.</p>
                            </li>
                        </ul>
                        <h2>Children’s Information</h2>
                        <p>Another part of our priority is adding protection for children while using the internet. We
                            encourage parents and guardians to observe, participate in, and/or monitor and guide their
                            online activity.</p>
                        <p>serviceprovider.ae does not knowingly collect any Personal Identifiable Information from children
                            under the age of 13. If you think that your child provided this kind of information on our
                            website, we strongly encourage you to contact us immediately and we will do our best efforts to
                            promptly remove such information from our records.</p>
                        <h2>Delivery Policy</h2>
                        <p><strong>1. General Information</strong></p>
                        <p>All orders are subject to product /service availability. If an item is not in stock at the time
                            you place your order, we will notify you and refund you the total amount of your order, using
                            the original method of payment.</p>
                        <p><strong>2. Delivery Location</strong></p>
                        <p>Items offered on our website are only available for delivery to addresses in UAE.</p>
                        <p><strong>3. Delivery Time</strong></p>
                        <p>An estimated delivery time will be provided to you once your order is placed. Delivery times are
                            estimates and commence from the date of shipping, rather than the date of order. Delivery times
                            are to be used as a guide only and are subject to the acceptance and approval of your order.</p>
                        <p>Unless there are exceptional circumstances, we make every effort to fulfill your order within 3
                            business days of the date of your order. Business day mean saturday to thurday, except holidays.
                        </p>
                        <p>Please note we do not ship on Friday.</p>
                        <h2>Order Cancellation</h2>
                        <p>A Customer can opt to cancel order within 2 hour for the order placed either online or by calling
                            our customer service and receive refund of advance paid if any. This is not applicable for
                            Express delivery orders since those are processed and shipped immediately.</p>
                        <p>One India Group reserves the right to cancel any order if it suspects any fraudulent transaction
                            by a Customer or breaches the terms &amp; conditions of using the website.</p>
                        <h2>Return &amp; Refunds</h2>
                        <p>Service provider believes in Centum Customer satisfaction and “no questions asked return and
                            refund policy” for issues relating to quality or freshness of its supplies. Given that the
                            orders are lower in size and its hard to refund for cash on delivery orders in particular, the
                            refunds will be applied to the subsequent shopping bills.</p>
                        <p>&nbsp;</p>
                    </div>
                    <!-- Left part END -->
                </div>
            </div>
        </div>
        <!-- Left & right section  END -->
    </div>
@endsection
