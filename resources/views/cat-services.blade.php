@extends('layouts.master')
@section('top-styles')
    <style>
        .ftco-animate {
            opacity: 0;
            visibility: hidden;
            margin-bottom: 25px !important;
            cursor: pointer;
        }

        .staff .text .position {
            font-size: 10px !important;
        }

        .staff:hover .text .position,
        .staff:focus .text .position {
            color: rgba(0, 0, 0, 0.5) !important;
        }

    </style>
@endsection
@section('content')
    <div class="hero-wrap js-fullheight" style="background-image: url('{{ url('') }}/assets/images/bg_1.jpg');"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start"
                data-scrollax-parent="true">
                <div class="col-md-6 ftco-animate">
                    <h2 class="subheading">Leave the house cleaning chores to us</h2>
                    <h1 class="mb-4">Let us do the dirty work, so you don't have to.</h1>
                    <p><a href="#" class="btn btn-primary mr-md-4 py-2 px-4">Learn more <span
                                class="ion-ios-arrow-forward"></span></a></p>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-appointment ftco-section ftco-no-pt ftco-no-pb">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex justify-content-center">
                <div class="col-md-12">
                    <div class="wrap-appointment bg-white d-md-flex pl-md-4 pb-5 pb-md-0">
                        <form action="#" class="appointment w-100">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md d-flex align-items-center pt-4 pt-md-0">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <input type="text" class="form-control" placeholder="Keyword">
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                <div class="icon"><span class="fa fa-chevron-down"></span></div>
                                                <select name="" id="" class="form-control">
                                                    <option value="">Select Services</option>
                                                    @foreach ($services as $d_service)
                                                        <option value="{{ $d_service->id }}">{{ $d_service->service }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center pb-4 pb-md-0">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                <div class="icon"><span class="fa fa-chevron-down"></span></div>
                                                <select name="" id="" class="form-control">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countrys as $country)
                                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center align-items-stretch">
                                    <div class="form-group px-4 px-md-0 d-flex align-items-stretch bg-primary">
                                        <input type="submit" value="Find Services" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section ftco-no-pb">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-12 heading-section  text-center ftco-animate">
                    <span class="subheading">Best Service Deals</span>
                    <h2>{{ $service->service }}</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                @foreach ($service->serviceDetails as $detail)
                    <div class="col-md-5 col-lg-4 ftco-animate d-flex">
                        <div class="staff">
                            <div class="img-wrap d-flex align-items-stretch">
                                <div class="img align-self-stretch"
                                    style="background-image: url('https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $detail->file }}');">
                                </div>
                            </div>
                            <div class="text pt-3 px-3 pb-4 text-center">
                                <h3 style="font-size: 14px;">{{ $detail->service }}</h3>
                                <span class="position mb-2">{{ $detail->description }}</span>
                                <span class="position mb-2"
                                    style="color: #f3e53d; font-weight: bold; font-size: 15px !important;">AED
                                    {{ $detail->rate }} / Person</span>
                                @if ($detail->book == 1)
                                    <a style="display: block"
                                        href="{{ route('booking_form', [app()->getLocale(),$detail->id]) }}">
                                        <button class="btn btn-warning">Book Now</button>
                                    </a>
                                @elseif($detail->appointment == 1)
                                    <a style="display: block"
                                        href="{{ route('booking.service', [app()->getLocale(),$detail->id]) }}">
                                        <button class="btn btn-success">Appointment</button>
                                    </a>
                                @else
                                    <a style="display: block"
                                        href="{{ route('qoute_form', [app()->getLocale(),$detail->id]) }}">
                                        <button class="btn btn-warning">Get Quote</button>
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
    </section>
@endsection
