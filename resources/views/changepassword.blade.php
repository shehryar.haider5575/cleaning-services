@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px; margin-bottom:10px;  background-color: #111;"></div>
    @include('layouts.dashboard-menu')
    <section class="py-4">
        <div class="container py-4">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4 class="h3 text-primary">Change Password</h4>
                </div>
                <div class="offset-2 col-md-8">
                    <form method="post" id="" class=""
                        action="{{ route('change.password', app()->getLocale()) }}">
                        @csrf
                        <div class="form-group" style="margin-top:5%;">
                            <label>New Password</label>
                            <input type="password" class="form-control customer-input" id="spassword"
                                placeholder="Enter Your Password" name="password" value="">
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input class="btn btn-primary btn-block" type="submit" value="Change Password"
                                    name="ChangepasswordBtn">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-2"></div>

            </div>
        </div>
    </section>
    </div>
@endsection
