@extends('layouts.master')
@section('top-styles')
@section('top-styles')
    <style>
        .ftco-animate {
            opacity: 0;
            visibility: hidden;
            margin-bottom: 25px !important;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
    <section>
        <div class="container">
            <div class="row py-4">
                <div class="col-md-2"></div>
                <div class="col-md-8 bg-light py-4">

                    <h2 class="text-center">Welcome to Service Provider</h2>

                    <p class="text-center">Please login to continue booking service to fulfil your requirement.</p>

                    <div class="modal-body clearfix">
                        <div class="row justify-content-center">
                            <form method="post" id="sp-login-form" class="job-manager-form" action="{{ route('login') }}"
                                style="width:100%;">
                                @csrf
                                <div class="sf-login-note-wrap"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="create_account_email">Your email</label>
                                        <input type="text" class="form-control" name="customeremail" id="customeremail"
                                            placeholder="you@yourdomain.com" value="" maxlength="" required="">
                                        <span class="text-danger">{{ $errors->first('customeremail') ?? null }}</span>
                                    </div>
                                    </fieldset>
                                    <div class="form-group">
                                        <label for="create_account_password">Your Password</label>
                                        <input type="password" class="form-control" name="customerpassword"
                                            id="customerpassword" placeholder="Password" value="" maxlength="" required="">
                                        <span
                                            class="text-danger">{{ $errors->first('customerpassword') ?? null }}</span>

                                    </div>
                                    </fieldset>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-left ml-3">
                                        <small class="switchSign"><a href="#" class="showSignup">Forgot Password?
                                            </a>|
                                            <a href="{{ route('register') }}" class="showSignup">Don't have an
                                                account?
                                            </a></small>
                                        <p class="sperrmsg"></p>
                                        <input id="spbtn" class="btn btn-primary float-left px-4" type="submit"
                                            value="Sign In" name="signInBtn">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
                <div class="col-md-2"></div>

            </div>
        </div>
    </section>
@endsection
