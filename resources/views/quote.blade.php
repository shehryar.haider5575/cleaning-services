@extends('layouts.master')
@section('content')
    <section>
        <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
        <div class="aboutus-secktion paddingTB60">
            <div class="container">
                <div class="row py-4">
                    <div class="col-md-8 bg-light">
                        <form method="post" id="customer-signup-form myForm" class="job-manager-form"
                            action="{{ route('quote-booking', app()->getLocale()) }}">
                            @csrf
                            <div class="bg-dark text-light px-3 py-2">Get Quote - {{ $service->service }} </div>
                            <div class="row" style="margin-top:5%;">
                                <input type="hidden" class="form-control customer-input" id="service"
                                    value="Luxury Car hire " required="" readonly="">
                                <input type="hidden" class="form-control customer-input" id="service_id" name="service_id"
                                    value="{{ $service->id }}">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name<span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter Your Name " id="name"
                                            name="name" value="{{ Auth::user()->customername }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contact Number<span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" placeholder=" Enter Your Number"
                                            id="cnumber" name="cnumber" required="" value="{{ Auth::user()->customerphonenumber }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Time For Call Back<span style="color:red;">*</span></label>
                                        <select class="form-control bs-select-hidden" id="ctime" name="ctime" required>
                                            <option selected="" disabled>--please select--</option>
                                            <option value="00:00 to 01:00">00:00 to 01:00</option>
                                            <option value="01:00 to 02:00">01:00 to 02:00</option>
                                            <option value="02:00 to 03:00">02:00 to 03:00</option>
                                            <option value="03:00 to 04:00">03:00 to 04:00</option>
                                            <option value="04:00 to 05:00">04:00 to 05:00</option>
                                            <option value="05:00 to 06:00">05:00 to 06:00</option>
                                            <option value="06:00 to 07:00">06:00 to 07:00</option>
                                            <option value="07:00 to 08:00">07:00 to 08:00</option>
                                            <option value="08:00 to 09:00">08:00 to 09:00</option>
                                            <option value="09:00 to 10:00">09:00 to 10:00</option>
                                            <option value="10:00 to 11:00">10:00 to 11:00</option>
                                            <option value="11:00 to 12:00">11:00 to 12:00</option>
                                            <option value="12:00 to 13:00">12:00 to 13:00</option>
                                            <option value="13:00 to 14:00">13:00 to 14:00</option>
                                            <option value="14:00 to 15:00">14:00 to 15:00</option>
                                            <option value="15:00 to 16:00">15:00 to 16:00</option>
                                            <option value="16:00 to 17:00">16:00 to 17:00</option>
                                            <option value="17:00 to 18:00">17:00 to 18:00</option>
                                            <option value="18:00 to 19:00">18:00 to 19:00</option>
                                            <option value="19:00 to 20:00">19:00 to 20:00</option>
                                            <option value="20:00 to 21:00">20:00 to 21:00</option>
                                            <option value="21:00 to 22:00">21:00 to 22:00</option>
                                            <option value="22:00 to 23:00">22:00 to 23:00</option>
                                            <option value="23:00 to 00:00">23:00 to 00:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Email Address<span style="color:red;">*</span></label>
                                        <input type="email" class="form-control" id="cemail"
                                            placeholder=" Enter Your Email" name="email" required="" value="{{ Auth::user()->customeremail }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Comments(if any)</label>
                                        <textarea class="form-control" placeholder=" Enter comment" id="comment" name="comment"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input class="btn btn-primary btn-block my-4" type="submit" value="Proceed to Checkout"
                                        name="AppointBtn" id="AppointBtn">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <div class="sf-featured-bx item">
                                <div class="sf-element-bx">
                                    <div class="sf-thum-bx img-effect2">
                                        <a href="#"
                                            style="background:url('https://www.serviceprovider.ae/beta/sp1/public/uploads/istockphoto-1215251260-170667a.jpg');height: 200px;background-size: cover;background-position: 50%;display: block;">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-text">
                                                <div class="sf-address-bx">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="sf-category-tag"><a
                                                href="#">Pest Control &amp;
                                                Disinfection </a></strong>
                                    </div>
                                    <div class="padding-20 bg-white">
                                        <h4 class="sf-title">
                                            <a href="#">1BR Apartment Pest
                                                Control</a>
                                        </h4>
                                        <p>1BR Apartment Pest Control Service</p>
                                    </div>
                                    <div class="btn-group-justified" id="proid-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="sf-featured-bx item">
                                <div class="sf-element-bx">
                                    <div class="sf-thum-bx img-effect2">
                                        <a href="#"
                                            style="background:url('https://www.serviceprovider.ae/beta/sp1/public/uploads/visa-3653492__480 (1).webp');height: 200px;background-size: cover;background-position: 50%;display: block;">
                                        </a>
                                        <div class="overlay-bx">
                                            <div class="overlay-text">
                                                <div class="sf-address-bx">
                                                </div>
                                            </div>
                                        </div>
                                        <strong class="sf-category-tag"><a
                                                href="#">Dubai Visa
                                                Package</a></strong>
                                    </div>
                                    <div class="padding-20 bg-white">
                                        <h4 class="sf-title">
                                            <a href="#">2 Years Residency
                                                Visa</a>
                                        </h4>
                                        <p>Get An Emirates ID For 2 Years</p>
                                    </div>
                                    <div class="btn-group-justified" id="proid-20">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
