@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px;background-color: black"></div>
    <div class="page-content">
        <!-- contact area -->
        <div class="container">
            <div class="section-content py-4">
                <div class="row">
                    <!-- Contact form start -->
                    <div class="col-md-9">
                        <h4>
                            Contact-page</h4>
                        <div class="padding-30 bg-white clearfix margin-b-30 sf-rouned-box">
                            <form method="post" class="contactform bv-form" novalidate="novalidate"><button type="submit"
                                    class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="input-group">
                                                <input name="uname" type="text" class="form-control" placeholder="Name"
                                                    data-bv-field="uname">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="input-group">
                                                <input name="email" type="text" class="form-control" placeholder="Email"
                                                    data-bv-field="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input name="phone" type="text" class="form-control" placeholder="Phone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <div class="input-group">
                                                <textarea name="comment" rows="4" class="form-control" placeholder="Comments" data-bv-field="comment"></textarea>
                                            </div><i class="form-control-feedback bv-no-label bv-icon-input-group"
                                                data-bv-icon-for="comment" style="display: none;"></i>
                                            <small class="help-block" data-bv-validator="notEmpty" data-bv-for="comment"
                                                data-bv-result="NOT_VALIDATED" style="display: none;">Please insert
                                                comment</small>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input name="submit" type="submit" value="Submit"
                                            class="btn btn-primary margin-r-10">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Contact form end -->
                    <!-- Contact info start -->
                    <div class="col-md-3">
                        <h4>Contact Info</h4>
                        <div class="padding-30 bg-white margin-b-30 sf-rouned-box">
                            <ul class="list-group contact-info no-margin">
                                <li class="list-group-item">
                                    <h6 class="text-primary"> Address </h6>
                                    <p>3001 - Moosa Tower 2 Sheikh Zayed Road - Dubai - United Arab Emirates.</p>
                                </li>
                                <li class="list-group-item">
                                    <h6 class="text-primary">
                                        EMAIl </h6>
                                    <p>spro.ae@gmail.com</p>
                                </li>
                                <li class="list-group-item">
                                    <h6 class="text-primary">
                                        PHONE </h6>
                                    <p>+971501170890</p>
                                </li>
                            </ul>
                            <br>
                        </div>
                    </div>
                    <!-- Contact info end -->

                </div>
            </div>
        </div>
        <!-- contact area  END -->
    </div>
@endsection
