<!DOCTYPE html>
<html>

<head>
    <title>spro.ae@gmail.com</title>
</head>

<body>
    <p>Hello,</p><br>
    <p>There is a new appointment booking done in wesite. please find the details in admin panel.</p>
    <table>
        <tr>
            <td>Appointment ID :</td>
            <td>{{ $details['app_id'] }}</td>
        </tr>
        <tr>
            <td>service name :</td>
            <td>{{ $details['service_name'] }}</td>
        </tr>
        <tr>
            <td>Address :</td>
            <td>{{ $details['address'] }}</td>
        </tr>
        <tr>
            <td>Email :</td>
            <td></td>
        </tr>
        <tr>
            <td>Phone :</td>
            <td>{{ $details['contact'] }}</td>
        </tr>
        <tr>
            <td>Date :</td>
            <td>{{ $details['adate'] }}</td>
        </tr>
        <tr>
            <td>Time :</td>
            <td>{{ $details['time'] }}</td>
        </tr>
        <tr>
            <td>Amount :</td>
            <td>{{ $details['rate'] }}</td>
        </tr>
    </table>
</body>

</html>
