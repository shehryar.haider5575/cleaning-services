<!DOCTYPE html>
<html>

<head>
    <title>spro.ae@gmail.com</title>
</head>

<body>
    <p>Hello,</p><br>
    <p>There is a new booking done in wesite. please find the details in admin panel.</p>
    <table>
        <tr>
            <td>service name :</td>
            <td>{{ $details['service_name'] }}</td>
        </tr>
        <tr>
            <td>Address :</td>
            <td>{{ $details['address'] }}</td>
        </tr>
        <tr>
            <td>Contact :</td>
            <td>{{ $details['contact'] }}</td>
        </tr>
        <tr>
            <td>Time :</td>
            <td>{{ $details['work_time'] }}</td>
        </tr>
        <tr>
            <td>Rate :</td>
            <td>{{ $details['rate'] }}</td>
        </tr>
        <tr>
            <td>Comments :</td>
            <td>{{ $details['comment'] }}</td>
        </tr>
    </table>
</body>

</html>
