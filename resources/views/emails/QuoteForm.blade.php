<!DOCTYPE html>
<html>

<head>
    <title>spro.ae@gmail.com</title>
</head>

<body>
    <p>Hello,</p><br>
    <p>There is a new Quotation booking done in wesite. please find the details in admin panel.</p>
    <table>
        <tr>
            <td>service name :</td>
            <td>{{ $details['service_name'] }}</td>
        </tr>
        <tr>
            <td>Time :</td>
            <td>{{ $details['ctime'] }}</td>
        </tr>
        <tr>
            <td>Email :</td>
            <td>{{ $details['email'] }}</td>
        </tr>
        <tr>
            <td>Phone :</td>
            <td>{{ $details['cnumber'] }}</td>
        </tr>
        <tr>
            <td>Comments :</td>
            <td>{{ $details['comment'] }}</td>
        </tr>
    </table>
</body>

</html>
