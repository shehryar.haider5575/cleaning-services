@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
    <div class="page-content">
        <div class="section-content profiles-content">
            <div class="container">
                <div class="row my-4">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="no-aacess-head col-md-12 my-4">
                                <div class="no-aacess-title text-primary text-center h3">
                                    <strong>Thank you. We have received your request to book an appointment.
                                        We will get back to you very soon</strong>
                                </div>
                            </div>
                            <div class="no-aacess-pic offset-3 col-md-9">
                                <img src="{{ asset('assets/img/success.png') }}" alt="">
                            </div>
                        </div>
                        <a href="{{ route('home', app()->getLocale()) }}" class="btn btn-primary">
                            GO TO SERVICES </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
