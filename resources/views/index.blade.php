@extends('layouts.master')
@section('top-styles')
    <style>
        .ftco-animate {
            opacity: 0;
            visibility: hidden;
            margin-bottom: 25px !important;
            cursor: pointer;
        }
        .service img {
            border-radius: 15px !important;
            height: 260px !important;
        }
        .service12 {
            height: 500px !important;
        }
        .btn-ser{
            position: absolute !important;
            bottom: 25px !important;
            
        }
        .service12{
            margin-top: 20px !important;
            margin-bottom: 0px !important;
        }
        .pb-5, .py-5 {
           padding-bottom: 0rem !important;
        }
    </style>
@endsection
@section('content')
    <div class="hero-wrap js-fullheight" style="background-image: url('{{ url('') }}/assets/images/bg_11.png');"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start"
                data-scrollax-parent="true">
                <div class="col-md-6 ftco-animate">
                    <h2 class="subheading">{{ __('Leave the house cleaning chores to us') }}</h2>
                    <h1 class="mb-4">{{ __("Let us do the dirty work, so you don't have to.") }}</h1>
                    <p class="ban-des">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean</p>
                    <p><a href="#" class="btn btn-primary mr-md-4 py-2 px-4">{{ __('LEARN MORE') }} <span
                                class="ion-ios-arrow-forward"></span></a></p>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-appointment ftco-section ftco-no-pt ftco-no-pb">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex justify-content-center">
                <div class="col-md-12">
                    <div class="wrap-appointment bg-white d-md-flex pl-md-4 pb-5 pb-md-0">
                        <form action="{{ route('services', app()->getLocale()) }}" method="POST"
                            class="appointment w-100">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-12 col-md d-flex align-items-center pt-4 pt-md-0">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <input type="text" class="form-control" placeholder="Keyword" name="keyword">
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                <div class="icon"><span class="fa fa-chevron-down"></span></div>
                                                <select name="service" id="" class="form-control">
                                                    <option value="">Select Services</option>
                                                    @foreach ($services as $service)
                                                        <option value="{{ $service->id }}">{{ $service->service }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center pb-4 pb-md-0">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                <div class="icon"><span class="fa fa-chevron-down"></span></div>
                                                <select name="country" id="" class="form-control">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countrys as $country)
                                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center align-items-stretch">
                                    <div class="form-group px-4 px-md-0 d-flex align-items-stretch bg-primary">
                                        <input type="submit" value="Find Services" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <section class="ftco-section ftco-no-pt ftco-no-pb">
        <div class="container">
            <div class="row d-flex no-gutters">
                <div class="col-md-6 d-flex">
                    <div class="img d-flex align-items-center justify-content-center py-5 py-md-0"
                        style="background-image:url({{ url('') }}/assets/images/about.jpg);">
                        <div class="time-open-wrap">
                            <div class="desc p-4">
                                <h2>Business Hours</h2>
                                <div class="opening-hours">
                                    <h4>Opening Days:</h4>
                                    <p class="pl-3">
                                        <span><strong>Monday – Monday:</strong> 24 / 7</span>
                                    </p>
                                    <h4>Vacations:</h4>
                                    <p class="pl-3">
                                        <span>All Sunday Days</span>
                                        <span>All Official Holidays</span>
                                    </p>
                                </div>
                            </div>
                            <div class="desc p-4 bg-secondary">
                                <h3 class="heading">For Emergency Cases</h3>
                                <span class="phone">+971501170890</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pl-md-5 pt-md-5">
                    <div class="row justify-content-start py-5">
                        <div class="col-md-12 heading-section ftco-animate">
                            <span class="subheading">Welcome to Cleaning Company</span>
                            <h2 class="mb-4">Let's make you fresher than ever</h2>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live the blind texts. Separated they live in Bookmarksgrove right at the coast of
                                the Semantics, a large language ocean. A small river named Duden flows by their place
                                and supplies it with the necessary regelialia. It is a paradisematic country, in which
                                roasted parts of sentences fly into your mouth.</p>
                        </div>
                    </div>
                    <div class="row ftco-counter py-5" id="section-counter">
                        <div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="45">0</strong>
                                </div>
                                <div class="text">
                                    <span>Years of <br>Experienced</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="2342">0</strong>
                                </div>
                                <div class="text">
                                    <span>Happy <br>Customers</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18">
                                <div class="text">
                                    <strong class="number" data-number="30">0</strong>
                                </div>
                                <div class="text">
                                    <span>Building <br>Cleaned</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


<section class="service-icon">
    
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="service-icon">
                <img src="{{ asset('assets/images/icon1.png') }}" width="30%" />
            </div>

            <div class="service-title">
               Professional and trusted
            </div>

            <div class="service-des">
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="service-icon">
                <img src="{{ asset('assets/images/icon-2.png') }}" width="30%" />
            </div>

            <div class="service-title">
               200% Satisfication
            </div>

            <div class="service-des">
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="service-icon">
                <img src="{{ asset('assets/images/icon-3.png') }}" width="30%" />
            </div>

            <div class="service-title">
                Book in 60 Seconf
            </div>

            <div class="service-des">
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="service-icon">
                <img src="{{ asset('assets/images/icon1.png') }}" width="30%" />
            </div>

            <div class="service-title">
                title
            </div>

            <div class="service-des">
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
            </div>
        </div>
    </div>
</div>

</section>


    <section class="offer">
        
        <div class="container">
            <div class="row">
                    <div class="col-sm-1">
                        <div class="count">
                            1
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="offer-desc">
                            <h5>We offer peace og mind.</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis labore ullam eum autem dolore molestiae beatae consectetur nulla minima ratione, similique voluptatem, enim nihil possimus, quam quae repellendus. Ut, maxime.</p>
                        </div>
                    </div>
                    <div class="col-sm-4"> <img src="{{ asset('assets/images/s1.png') }}" width="100%" /></div>

                    
                    <div class="col-sm-4"> <img src="{{ asset('assets/images/s2.png') }}" width="100%" /></div>
                    <div class="col-sm-1">
                        <div class="count">
                            2
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="offer-desc">
                            <h5>We offer conveience and simplicty</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis labore ullam eum autem dolore molestiae beatae consectetur nulla minima ratione, similique voluptatem, enim nihil possimus, quam quae repellendus. Ut, maxime.</p>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="count">
                            3
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="offer-desc">
                            <h5>We work with Eco Friendly Products</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis labore ullam eum autem dolore molestiae beatae consectetur nulla minima ratione, similique voluptatem, enim nihil possimus, quam quae repellendus. Ut, maxime.</p>
                        </div>
                    </div>
                    <div class="col-sm-4"> <img src="{{ asset('assets/images/s3.png') }}" width="100%" /></div>
            </div>
        </div>
        
    </section>

    <section class="ftco-section" style="padding-bottom: 0px">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-7 heading-section text-center ftco-animate">
                    <h2>Why Choose us</h2>
                    <span class="subheading text-dark">Serviceprovider.ae is one of the largest service provider portals in
                        the
                        region. It is a one-stop solution for all your home, office, health, lifestyle, and on the road
                        needs.... We simplify your everyday living with a huge variety of at-home services across various
                        cities, countries, and regions. All work is conducted with due diligence by our verified, qualified,
                        and carefully chosen service providers in the city. The portal is managed and operated by service
                        industry experts with decades of service industry experience. Let's build the services world
                        together.</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 services ftco-animate">
                    <div class="d-block d-flex">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{ asset('assets/img/handshake.png') }}" width="50" />
                        </div>
                        <div class="media-body pl-3">
                            <h3 class="heading">MEET NEW CUSTOMERS</h3>
                            <p>We help you find new customers in your city and expand your business.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 services ftco-animate">
                    <div class="d-block d-flex">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{ asset('assets/img/growth.png') }}" width="50" />
                        </div>
                        <div class="media-body pl-3">
                            <h3 class="heading">GROW YOUR REVENUE</h3>
                            <p>More customers means more revenue and ensure consistent orders .</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 services ftco-animate">
                    <div class="d-block d-flex">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{ asset('assets/img/build.png') }}" width="50" />
                        </div>
                        <div class="media-body pl-3">
                            <h3 class="heading">BUILD YOUR ONLINE REPUTATION</h3>
                            <p>We help you improve your online reputation and help you build long term relations.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

<!--     <section class="ftco-section ftco-no-pb" style="padding-top: 0px">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-12 heading-section  text-center ftco-animate">
                    <span class="subheading">Best Service Deals</span>
                    <h2>Service Provider</h2>
                </div>
            </div>
            <div class="row">
                @foreach ($services as $ss)
                    <div class="col-md-5 col-lg-4 ftco-animate d-flex">
                        <div class="staff">
                            <a href="{{ route('service.detail', [app()->getLocale() , $ss->id ]) }}">
                                <div class="img-wrap d-flex align-items-stretch">
                                    <div class="img align-self-stretch"
                                        style="background-image: url('https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $ss->file }}');">
                                    </div>
                                </div>
                                <div class="text pt-3 px-3 pb-4 text-center">
                                    <h3 style="    font-size: 12px;">{{ $ss->service }}</h3>
                                    {{-- <span class="position mb-2">Office Cleaner</span> --}}

                                </div>
                            </a>
                        </div>
                    </div>
                    {{-- <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="work img d-flex align-items-center"
                            style="background-image: url('https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $ss->file }}')">
                            <a href="https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $ss->file }}"
                                class="icon image-popup d-flex justify-content-center align-items-center">
                                <span class="fa fa-expand"></span>
                            </a>
                            <div class="desc w-100 px-4 text-center pt-5 mt-5">
                                <div class="text w-100 mb-3 mt-4">
                                    <h2><a href="work-single.html">{{ $ss->service }}</a></h2>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                @endforeach
            </div>
    </section> -->

    <section class="ftco-section ftco-no-pb1" style="padding-top: 0px">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-12 heading-section  text-center ftco-animate">
                    <span class="subheading">Best Service Deals</span>
                    <h2>All Service Category</h2>
                </div>
            </div>
            <div class="row">
                @foreach ($services as $ss)

                    <div class="col-sm-4 card-serv">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $ss->file }}" width="100%" />
                         <h5>{{ $ss->service }}</h5>
                         <p>{{ $ss->slug }}</p>
                         <a href="{{ route('service.detail', [app()->getLocale() , $ss->id ]) }}"><button class="btn-ser">BOOK NOW</button></a>
                         </div>
                    </div>
                @endforeach

                    {{-- <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/corona%20red%20(1).png" width="100%" />
                         <h5>COVID SELF TESTING KITS</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>
                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/Custom%20dimensions%20943x788%20px.jpeg" width="100%" />
                         <h5>Pest Control & Disinfection  </h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/Custom%20dimensions%20941x789%20px%20(2).jpeg" width="100%" />
                         <h5>Dubai Visa Package</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/gettyimages-184280978-612x612.jpg" width="100%" height="200%" />
                         <h5>Luxury Transportation Rentals</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <br>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/Custom%20dimensions%20942x786%20px.jpeg" width="100%" />
                         <h5>Website Development</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div> --}}


            </div>
    </section>

    <section class="ftco-section testimony-section ftco-bg-dark">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-7 heading-section heading-section-white text-center ftco-animate">
                    <span class="subheading">Testimonies</span>
                    <h2>Happy Customer</h2>
                </div>
            </div>
            <div class="row ftco-animate">
                <div class="col-md-12">
                    <div class="carousel-testimony owl-carousel ftco-owl">
                        <div class="item">
                            <div class="testimony-wrap py-4">
                                <div class="icon d-flex align-items-center justify-content-center"><span
                                        class="fa fa-quote-right"></span></div>
                                <div class="text">
                                    <div class="d-flex align-items-center mb-4">
                                        <div class="user-img"
                                            style="background-image: url({{ url('') }}/assets/images/person_1.jpg)">
                                        </div>
                                        <div class="pl-3">
                                            <p class="name">Roger Scott</p>
                                            <span class="position">Marketing Manager</span>
                                        </div>
                                    </div>
                                    <p class="mb-1">Far far away, behind the word mountains, far from the
                                        countries
                                        Vokalia and Consonantia, there live the blind texts.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap py-4">
                                <div class="icon d-flex align-items-center justify-content-center"><span
                                        class="fa fa-quote-right"></span></div>
                                <div class="text">
                                    <div class="d-flex align-items-center mb-4">
                                        <div class="user-img"
                                            style="background-image: url({{ url('') }}/assets/images/person_2.jpg)">
                                        </div>
                                        <div class="pl-3">
                                            <p class="name">Roger Scott</p>
                                            <span class="position">Marketing Manager</span>
                                        </div>
                                    </div>
                                    <p class="mb-1">Far far away, behind the word mountains, far from the
                                        countries
                                        Vokalia and Consonantia, there live the blind texts.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap py-4">
                                <div class="icon d-flex align-items-center justify-content-center"><span
                                        class="fa fa-quote-right"></span></div>
                                <div class="text">
                                    <div class="d-flex align-items-center mb-4">
                                        <div class="user-img"
                                            style="background-image: url({{ url('') }}/assets/images/person_3.jpg)">
                                        </div>
                                        <div class="pl-3">
                                            <p class="name">Roger Scott</p>
                                            <span class="position">Marketing Manager</span>
                                        </div>
                                    </div>
                                    <p class="mb-1">Far far away, behind the word mountains, far from the
                                        countries
                                        Vokalia and Consonantia, there live the blind texts.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap py-4">
                                <div class="icon d-flex align-items-center justify-content-center"><span
                                        class="fa fa-quote-right"></span></div>
                                <div class="text">
                                    <div class="d-flex align-items-center mb-4">
                                        <div class="user-img"
                                            style="background-image: url({{ url('') }}/assets/images/person_1.jpg)">
                                        </div>
                                        <div class="pl-3">
                                            <p class="name">Roger Scott</p>
                                            <span class="position">Marketing Manager</span>
                                        </div>
                                    </div>
                                    <p class="mb-1">Far far away, behind the word mountains, far from the
                                        countries
                                        Vokalia and Consonantia, there live the blind texts.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-wrap py-4">
                                <div class="icon d-flex align-items-center justify-content-center"><span
                                        class="fa fa-quote-right"></span></div>
                                <div class="text">
                                    <div class="d-flex align-items-center mb-4">
                                        <div class="user-img"
                                            style="background-image: url({{ url('') }}/assets/images/person_2.jpg)">
                                        </div>
                                        <div class="pl-3">
                                            <p class="name">Roger Scott</p>
                                            <span class="position">Marketing Manager</span>
                                        </div>
                                    </div>
                                    <p class="mb-1">Far far away, behind the word mountains, far from the
                                        countries
                                        Vokalia and Consonantia, there live the blind texts.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
