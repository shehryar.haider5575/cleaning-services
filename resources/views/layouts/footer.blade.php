<footer class="footer ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-4 mb-4 mb-md-0">
                <h2 class="footer-heading">Cleaning Company</h2>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It
                    is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-4">
                    <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-4 pl-lg-5 mb-4 mb-md-0">
                <h2 class="footer-heading">Quick Links</h2>
                <ul class="list-unstyled">
                    <li><a href="{{ route('home', app()->getLocale()) }}" class="py-1 d-block">Home</a></li>
                    <li><a href="{{ route('about', app()->getLocale()) }}" class="py-1 d-block">About</a></li>
                    <li><a href="{{ route('contacts', app()->getLocale()) }}" class="py-1 d-block">Contact</a></li>
                    <li><a href="{{ route('policy', app()->getLocale()) }}" class="py-1 d-block">Privacy Policy</a>
                    </li>
                    <li><a href="#" class="py-1 d-block">Terms of Use</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-md-0">
                <h2 class="footer-heading">Have a Questions?</h2>
                <div class="block-23 mb-3">
                    <ul>
                        <li><span class="icon fa fa-map-marker"></span><span class="text">3001 - Moosa Tower 2
                                Sheikh Zayed Road - Dubai - United Arab Emirates.</span></li>
                        <li><a href="#"><span class="icon fa fa-phone"></span><span
                                    class="text">+971501170890</span></a></li>
                        <li><a href="#"><span class="icon fa fa-paper-plane"></span><span
                                    class="text">spro.ae@gmail.com</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 text-center">

                {{-- <p class="copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This template is made with <i class="fa fa-heart"
                        aria-hidden="true"></i> by <a href="http://www.ramshatechnologies.com/" target="_blank">Ramsha
                        Technologies</a>
                </p> --}}
            </div>
        </div>
    </div>
</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
            stroke="#F96D00" />
    </svg></div>
