<style>
    .active {
        border-bottom: 1px solid #05ed05;
        background-color: #7a7a7a;
    }

</style>
<nav class="navbar navbar-expand-lg navbar-light bg-dark p-0">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item px-3 {{ Route::currentRouteName() == 'user-dashboard' ? 'active' : '' }}"
                style="border-right: 1px solid white">
                <a class="nav-link text-white" href="{{ route('user-dashboard', app()->getLocale()) }}">dashboard</a>
            </li>
            <li class="nav-item px-3 {{ Route::currentRouteName() == 'user-myorder' ? 'active' : '' }}"
                style="border-right: 1px solid white">
                <a class="nav-link text-white" href="{{ route('user-myorder', app()->getLocale()) }}">My Orders</a>
            </li>
            <li class="nav-item px-3 {{ Route::currentRouteName() == 'user-myqoute' ? 'active' : '' }}"
                style="border-right: 1px solid white">
                <a class="nav-link text-white" href="{{ route('user-myqoute', app()->getLocale()) }}">My Quotes</a>
            </li>
            </li>
            <li class="nav-item px-3 {{ Route::currentRouteName() == 'user-myappointment' ? 'active' : '' }}"
                style="border-right: 1px solid white">
                <a class="nav-link text-white" href="{{ route('user-myappointment', app()->getLocale()) }}">My
                    Appointment</a>
            </li>
            <li class="nav-item px-3 {{ Route::currentRouteName() == 'forgetpassword' ? 'active' : '' }}"
                style="border-right: 1px solid white">
                <a class="nav-link text-white" href="{{ route('forgetpassword', app()->getLocale()) }}">Change
                    Password</a>
            </li>
            <li class="nav-item px-3">
                <a class="nav-link text-white" href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                    logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>
