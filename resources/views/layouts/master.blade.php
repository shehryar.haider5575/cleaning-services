<!DOCTYPE html>
<html lang="en">

<head>
    <title>Cleaning Company - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ url('') }}/assets/css/animate.css">

    <link rel="stylesheet" href="{{ url('') }}/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ url('') }}/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ url('') }}/assets/css/magnific-popup.css">

    <link rel="stylesheet" href="{{ url('') }}/assets/css/flaticon.css">
    <link rel="stylesheet" href="{{ url('') }}/assets/css/style.css">
</head>
@yield('top-styles')

<body>
    <!-- Header -->

    @include('layouts.loader')
    @include('layouts.header')

    <!-- Layout -->
    @yield('content')

    @include('layouts.footer')

    <!-- SCRIPTS -->

    <script src="{{ url('') }}/assets/js/jquery.min.js"></script>
    <script src="{{ url('') }}/assets/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="{{ url('') }}/assets/js/popper.min.js"></script>
    <script src="{{ url('') }}/assets/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/assets/js/jquery.easing.1.3.js"></script>
    <script src="{{ url('') }}/assets/js/jquery.waypoints.min.js"></script>
    <script src="{{ url('') }}/assets/js/jquery.stellar.min.js"></script>
    <script src="{{ url('') }}/assets/js/jquery.animateNumber.min.js"></script>
    <script src="{{ url('') }}/assets/js/owl.carousel.min.js"></script>
    <script src="{{ url('') }}/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="{{ url('') }}/assets/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="{{ url('') }}/assets/js/google-map.js"></script>
    <script src="{{ url('') }}/assets/js/main.js"></script>
    @yield('page-scripts')
    @yield('custom-script')

</body>

</html>
