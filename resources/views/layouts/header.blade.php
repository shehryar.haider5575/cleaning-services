<div class="wrap">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12 col-md d-flex align-items-center">
                <p class="mb-0 phone"><span class="mailus">Phone no:</span>
                    <a href="#">+971501170890</a>
                    or
                    <span class="mailus">email us:</span> <a href="#">spro.ae@gmail.com</a>
                </p>
            </div>
            <div class="col-12 col-md d-flex justify-content-md-end">
                <div class="social-media">
                    <p class="mb-0 d-flex">
                        <a href="#" class="d-flex align-items-center justify-content-center"><span
                                class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"><span
                                class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"><span
                                class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"><span
                                class="fa fa-dribbble"><i class="sr-only">Dribbble</i></span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home', app()->getLocale()) }}"><img
                src="{{ asset('assets/img/logo.png') }}" alt="logo" width="60" height="70"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
            aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a
                        href="{{ route('home', app()->getLocale()) }}" class="nav-link">Home</a></li>
                <li class="nav-item {{ Route::currentRouteName() == 'about' ? 'active' : '' }}"><a
                        href="{{ route('about', app()->getLocale()) }}" class="nav-link">About</a></li>
                <li class="nav-item {{ Route::currentRouteName() == 'services' ? 'active' : '' }}"><a
                        href="{{ route('services', app()->getLocale()) }}" class="nav-link">Services</a></li>
                <li class="nav-item {{ Route::currentRouteName() == 'dashboard' ? 'active' : '' }}"><a
                        href="{{ route('user-dashboard', app()->getLocale()) }}" class="nav-link">Account</a>
                </li>
                <li class="nav-item {{ Route::currentRouteName() == 'contacts' ? 'active' : '' }}"><a
                        href="{{ route('contacts', app()->getLocale()) }}" class="nav-link">Contact</a></li>
                <li class="nav-item">
                    <div class="dropdown" style="color: white;position: relative;top: -28px;right: -80px;">
                        <div class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Lang - {{ app()->getlocale() }}
                        </div>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('set-lang', 'en') }}">ENGLISH</a>
                            <a class="dropdown-item" href="{{ route('set-lang', 'arr') }}">ARABIC</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->
