@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
    @include('layouts.dashboard-menu')
    <div class="page-content">
        <div class="section-content profiles-content">
            <div class="container">
                <div class="row my-4">
                    <div class="col-md-12">
                        <h2 class="text-left">Welcome {{ auth()->user()->customername }}</h2>
                        <h6 class="text-left">Manage all your services here</h6>
                        <div class="row">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>Service</th>
                                        <th>Total Amount</th>
                                        <th>Order Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 0;
                                    @endphp
                                    @if (!empty($order))
                                        @foreach ($order as $odr)
                                            <tr>
                                                <th>{{ $i }}</th>
                                                <th>{{ !empty($odr->service->service) ? $odr->service->service : '' }}
                                                </th>
                                                <th>AED {{ $odr->rate }}</th>
                                                <th>{{ $odr->entry_date }}</th>
                                                <th>#</th>
                                            </tr>
                                            @php
                                                $i++;
                                            @endphp
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6">No Record Found</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
