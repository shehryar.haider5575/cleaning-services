@extends('layouts.master')
@section('top-styles')
    <style>
        .ftco-animate {
            opacity: 0;
            visibility: hidden;
            margin-bottom: 25px !important;
            cursor: pointer;
        }
        .service12 img {
            border-radius: 15px !important;
            height: 230px !important;
        }
        .service12 {
            height: 500px !important;
        }
        .btn-ser{
            position: absolute !important;
            bottom: 25px !important;
            
        }
        .service12{
            margin-top: 20px !important;
            margin-bottom: 0px !important;
        }
        .service12 h5{
            line-height: 1;
        }
        .pagination{
            margin: 70px 14px !important;
        }
        .pb-5, .py-5 {
           padding-bottom: 0rem !important;
        }
    </style>
@endsection
@section('content')
    <div class="hero-wrap js-fullheight" style="background-image: url('{{ url('') }}/assets/images/bg_11.png');"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start"
                data-scrollax-parent="true">
                <div class="col-md-6 ftco-animate">
                    <h2 class="subheading">{{ __('Leave the house cleaning chores to us') }}</h2>
                    <h1 class="mb-4">{{ __("Let us do the dirty work, so you don't have to.") }}</h1>
                    <p class="ban-des">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean</p>
                    <p><a href="#" class="btn btn-primary mr-md-4 py-2 px-4">{{ __('LEARN MORE') }} <span
                                class="ion-ios-arrow-forward"></span></a></p>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-appointment ftco-section ftco-no-pt ftco-no-pb">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex justify-content-center">
                <div class="col-md-12">
                    <div class="wrap-appointment bg-white d-md-flex pl-md-4 pb-5 pb-md-0">
                        <form action="{{ route('services', app()->getLocale()) }}" method="POST"
                            class="appointment w-100">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col-12 col-md d-flex align-items-center pt-4 pt-md-0">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <input type="text" class="form-control" placeholder="Keyword" name="keyword">
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                <div class="icon"><span class="fa fa-chevron-down"></span></div>
                                                <select name="service" id="" class="form-control">
                                                    <option value="">Select Services</option>
                                                    @foreach ($service as $ser)
                                                        <option value="{{ $ser->id }}">{{ $ser->service }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center pb-4 pb-md-0">
                                    <div class="form-group py-md-4 py-2 px-4 px-md-0">
                                        <div class="form-field">
                                            <div class="select-wrap">
                                                <div class="icon"><span class="fa fa-chevron-down"></span></div>
                                                <select name="country" id="" class="form-control">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countrys as $country)
                                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md d-flex align-items-center align-items-stretch">
                                    <div class="form-group px-4 px-md-0 d-flex align-items-stretch bg-primary">
                                        <input type="submit" value="Find Services" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

 <!--    <section class="ftco-section ftco-no-pb">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-12 heading-section  text-center ftco-animate">
                    <span class="subheading">Best Service Deals</span>
                    <h2>Service Provider</h2>
                </div>
            </div>
            <div class="row">
                @if ($services === null)
                    <h1 class="col-md-12 text-center pb-4">No Record Found</h1>
                @else
                    @foreach ($services as $ss)
                        <div class="col-md-5 col-lg-4 ftco-animate d-flex">
                            <div class="staff">
                                <div class="img-wrap d-flex align-items-stretch">
                                    <div class="img align-self-stretch"
                                        style="background-image: url('https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $ss->file }}');">
                                    </div>
                                </div>
                                <div class="text pt-3 px-3 pb-4 text-center">
                                    <h3 style="font-size: 14px;">{{ $ss->service }}</h3>
                                    <h5 style="font-size: 14px;">
                                        {{ !empty($ss->pservice) ? $ss->pservice->service : 'No Category' }}
                                    </h5>
                                    <span
                                        class="position mb-2">{{ !empty($ss->description) ? substr($ss->description, 0, 50) : 'No Description' }}</span><br>
                                    <span class="position mb-2"
                                        style="color:  #000000; font-weight: bold; font-size: 15px !important;">AED
                                        {{ $ss->rate }} / Person</span>
                                    @if ($ss->book == 1)
                                        <a style="display: block"
                                            href="{{ route('booking_form', [app()->getLocale(), $ss->id]) }}">
                                            <button class="btn btn-warning">Book Now</button>
                                        </a>
                                    @elseif($ss->appointment == 1)
                                        <a style="display: block"
                                            href="{{ route('booking.service', [app()->getLocale(), $ss->id]) }}">
                                            <button class="btn btn-success">Appointment</button>
                                        </a>
                                    @else
                                        <a style="display: block"
                                            href="{{ route('qoute_form', [app()->getLocale(), $ss->id]) }}">
                                            <button class="btn btn-warning">Get Quote</button>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
    </section> -->

<br> <br> <br>
    <section class="ftco-section ftco-no-pb1" style="padding-top: 0px">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-12 heading-section  text-center ftco-animate">
                    <span class="subheading">Best Service Deals</span>
                    <h2>All Services</h2>
                </div>
            </div>
            <div class="row">
                @if ($services === null)
                    <h1 class="col-md-12 text-center pb-4">No Record Found</h1>
                @else
                    @foreach ($services as $ss)

                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/{{ $ss->file }}" width="100%" />
                         <h5>{{ $ss->service }}</h5>
                         <h5>AED {{ $ss->rate }}</h5>
                         <p>{{ !empty($ss->description) ? substr($ss->description, 0, 50) : 'No Description' }} </p>
                         @if ($ss->book == 1)
                            <a href="{{ route('booking_form', [app()->getLocale(), $ss->id]) }}">
                                <button class="btn-ser">Book Now</button>
                            </a>
                        @elseif($ss->appointment == 1)
                            <a href="{{ route('booking.service', [app()->getLocale(), $ss->id]) }}">
                                <button class="btn-ser">Appointment</button>
                            </a>
                        @else
                            <a href="{{ route('qoute_form', [app()->getLocale(), $ss->id]) }}">
                                <button class="btn-ser">Get Quote</button>
                            </a>
                        @endif
                         {{-- <a href="#"><button>BOOK NOW</button></a> --}}
                         </div>
                    </div>
                    @endforeach
                    {{$services->links()}}
                @endif
                    {{-- <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/corona%20red%20(1).png" width="100%" />
                         <h5>COVID SELF TESTING KITS</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>
                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/Custom%20dimensions%20943x788%20px.jpeg" width="100%" />
                         <h5>Pest Control & Disinfection  </h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/Custom%20dimensions%20941x789%20px%20(2).jpeg" width="100%" />
                         <h5>Dubai Visa Package</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/gettyimages-184280978-612x612.jpg" width="100%" height="200%" />
                         <h5>Luxury Transportation Rentals</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <br>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div>
                    <div class="col-sm-4">
                         <div class="service12">
                         <img src="https://www.serviceprovider.ae/beta/sp1/public/uploads/Custom%20dimensions%20942x786%20px.jpeg" width="100%" />
                         <h5>Website Development</h5>
                         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua. </p>
                         <a href="#"><button>BOOK NOW</button></a>
                         </div>

                    </div> --}}


            </div>
    </section>
@endsection
