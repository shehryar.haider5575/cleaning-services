@extends('layouts.master')
@section('content')
    <div style="padding-top: 120px; margin-bottom:10px; background-color: black"></div>
    @include('layouts.dashboard-menu')
    <div class="page-content">
        <div class="section-content profiles-content">
            <div class="container">
                <div class="row my-4">
                    <div class="col-md-12">
                        <h2 class="text-left">Welcome {{ auth()->user()->customername }}</h2>
                        <h6 class="text-left">Manage all your services here</h6>
                        <div class="row">
                            <div class="no-aacess-head col-md-12 text-center">
                                <div class="h2 text-primary">Start searching for your needs</div>
                            </div>
                            <div class="col-md-12">
                                <img src="{{ asset('assets/img/dashobard.png') }}" alt="dashobard" class="w-100">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
