@extends('admin::layouts.master')
@section('title', 'Dashboard')

@section('top-styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
    rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
    type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet"
    type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }
</style>
@endsection
@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('dashboard')}}">ADMIN</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{route('products')}}">Products</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Add Product</span>
    </li>
</ul>
@endsection

<!-- BEGIN Products STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="icon-diamond"> </i> Add Product
                        <button onclick="window.history.back(1)" type="button"
                            class="btn btn-block btn-primary btn-md float-sm-right"
                            >Go
                            Back</button>
                    </div>
                    <div class="panel-body">
                        <!-- form start -->
                        <!-- BEGIN FORM-->
                        <!-- form start -->
                        <form action="{{$isEdit ? route('product.update',$product->id) : route('product.store')}}"
                            role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                            <input type="hidden" name="_method" value="put">
                            @endif
                            <input type="hidden" name="user_type" value="1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="avatar-upload" style="height: 200px;">
                                        <div class="avatar-edit">
                                            <input type='file' name="featured_image" id="imageUpload1"
                                                accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Featured Image </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/'}}{{$isEdit ? $product->featured_image : 'placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                    <span class="text-danger">{{$errors->first('featured_image') ?? null}}</span>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter Name" value="{{$product->name ?? old('name')}}">
                                        <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                                    </div>
                                    <div class="form-group" style="margin-top: 25px;">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            Click to Featured this Product
                                            <input type="checkbox" name="is_featured" class="mail-group-checkbox form-control" value="1" @if ($isEdit) {{$product->is_featured == 1 ? 'checked' : null}} @endif />
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="form-group" style="margin-top: 25px;">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            Click to show on Home Page
                                            <input type="checkbox" name="main_view" class="mail-group-checkbox form-control" value="1" @if ($isEdit) {{$product->main_view == 1 ? 'checked' : null}} @endif />
                                            <span></span>
                                        </label>
                                    </div>
                                    {{-- <div class="form-group">
                                            <label for="code">Code <span class="text-danger">*</span></label>
                                            <input type="text" name="code" class="form-control" id="code"
                                                placeholder="Enter Code" value="{{$product->code ?? old('code')}}">
                                    <span class="text-danger">{{$errors->first('code') ?? null}}</span>
                                </div> --}}
                                {{-- <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="price">Price <span class="text-danger">*</span></label>
                                            <input type="text" name="price" class="form-control" id="price"
                                                placeholder="Enter Price" value="{{$product->price ?? old('price')}}">
                                <span class="text-danger">{{$errors->first('price') ?? null}}</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="stock">Stock <span class="text-danger">*</span></label>
                                <input type="text" name="stock" class="form-control" id="stock"
                                    placeholder="Enter Stock" value="{{$product->stock ?? old('stock')}}">
                                <span class="text-danger">{{$errors->first('stock') ?? null}}</span>
                            </div>
                    </div> --}}
                </div>
                </br>
                <div class="col-md-12">
                        <p></p>
                        {{-- <div class="form-group col-md-4" style="margin-top: 25px;padding-left: 20px;">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                Click to Featured this Product
                                <input type="checkbox" name="is_featured" class="mail-group-checkbox form-control"
                                    value="1" />
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group col-md-4" style="margin-top: 25px;padding-left: 20px;">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                Click to show on Home Page
                                <input type="checkbox" name="main_view" class="mail-group-checkbox form-control"
                                    value="1" />
                                <span></span>
                            </label>
                        </div> --}}
                        <div class="form-group">
                            <label for="images">Product Sub Images</label>
                            <input type="file" multiple name="images[]" class="form-control" id="images">
                            <span class="text-danger">{{$errors->first('images') ?? null}}</span>
                        </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" cols="30" rows="10"
                            placeholder="Enter Product Description">
                                            @if ($isEdit)
                                            {{$product->description ?? old('description')}}
                                            @endif
                                        </textarea>
                        <span class="text-danger">{{$errors->first('description') ?? null}}</span>
                    </div>
                </div>

                <div class="section row">
                    @if ($isEdit)
                    @foreach ($product->productDetails as $item)
                    <div class="images col-md-2">
                        <a href="{{route('product_image.delete',$item->id)}}"><span class="fa fa-close"></span></a>
                        <img src="{{url('').'/uploads/'.$item->attachment}}" height='121px' width='190px'>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary button-custom">{{$isEdit ? 'Update' : 'Save'}}</button>
            </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<!-- END Page -->
@section('page-scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- jquery-validation -->
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/additional-methods.min.js"></script>

<script src="{{url('')}}/dash-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/additional-methods.min.js"
    type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
    type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#quickForm').validate({
            rules: {
                name: {
                    required: true,
                },
                code: {
                    required: true,
                },
                price: {
                    required: true,
                },
                stock: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');

            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });
    });
</script>
@endsection
@endsection