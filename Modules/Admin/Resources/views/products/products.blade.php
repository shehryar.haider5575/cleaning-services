@extends('admin::layouts.master')
@section('title', 'Products')

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">
       
<!-- Plugins css-->
<link href="{{url('')}}/dash-assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->
<style>
    a.btn.btn-secondary {
        background: #dc3535bf;
        border: 1px solid white;
    }
    h2#swal2-title {
        font-size: 25px;
    }
    div#swal2-content {
        font-size: 17px;
    }
    .swal2-icon.swal2-warning, .swal2-success-ring {
        border-radius: 50% !important;
        font-size: 11px;
    }
    .swal2-popup.swal2-modal.swal2-icon-warning.swal2-show {
        height: 280px;
        width: 36em;
    }
    table.dataTable.no-footer
    {
        border-bottom:inherit !important; 
    }
</style>
@endsection

@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('dashboard')}}">ADMIN</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>List Products</span>
    </li>
</ul>
@endsection

<!-- BEGIN Products STATS 1-->
<!-- BEGIN Products STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="icon-diamond"> </i> List Products
                        <a href="{{route('product.create')}}">
                            <button type="button" class="btn btn-block btn-primary btn-md float-sm-right" >Add Product</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="custom_datatable">
                            <form action="#" id="advanceSearch">
                                <div class="bg-black-transparent1 m-b-15 p15 pb0">
                                    <div class="row">
                                        {{-- <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Code</label>
                                                <input type="text" name="code" id="autocomplete-ajax1"
                                                    class="form-control" placeholder="Code" style=" z-index: 2;"
                                                    autocomplete="off">
                                            </div>
                                        </div> --}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Name</label>
                                                <input type="text" name="name" class="form-control"
                                                    placeholder="Name" style=" z-index: 2;" autocomplete="off">
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Date</label>
                                                <input type="date" name="date" class="form-control date"
                                                    style=" z-index: 2;" autocomplete="off">
                                            </div>
                                        </div> --}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Filter by Featured</label>
                                                <select parsley-trigger="change" data-style="btn-white"
                                                    name="is_featured" class="form-control featured">
                                                    <option selected="" value="">No Filter</option>
                                                    <option value="1">Featured Product</option>
                                                    <option value="0">Non Featured Product</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Filter by Status</label>
                                                <select parsley-trigger="change" data-style="btn-white"
                                                    name="status" class="form-control ver-status">
                                                    <option selected="" value="">No Filter</option>
                                                    <option value="1">Active</option>
                                                    <option value="0">Non Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-8"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <button id="search"
                                                    class="btn btn-light-theme btn-block waves-effect waves-light">
                                                    <i class="fa fa-search pr-1"></i> Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                 </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th class="all">S.No</th>
                                                <th class="min-phone-l">Featured Image</th>
                                                <th class="min-phone-l">Name</th>
                                                {{-- <th class="all">Code</th>
                                                <th class="min-tablet">Price</th>
                                                <th class="none">Stock</th> --}}
                                                <th class="none">Featured</th>
                                                <th class="desktop">Home Page</th>
                                                <th class="none">Created</th>
                                                <th class="all" width="10%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection

@section('rightsidebar')
@parent
@endsection

@section('page-scripts')
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

<script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#sample_1').DataTable({
            // retrieve: true,
            destroy: true,
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                'csv',
                {
                    text: 'PDF',
                    extend: 'pdfHtml5',
                    // filename: 'units',
                    orientation: 'landscape', //portrait
                    // download: 'open',
                    pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    exportOptions: {
                        columns: [0, 1],
                        search: 'applied',
                        order: 'applied'
                    },
                    customize: function (doc) {
                        doc.content[1].table.widths = ['10%', '90%'];
                        doc.styles.tableBodyEven.alignment = 'center';
                        doc.styles.tableBodyOdd.alignment = 'center';
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        //Create a date string that we use in the footer. Format is dd-mm-yyyy
                        var now = new Date();
                        var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now
                            .getFullYear();
                        doc.pageMargins = [20, 60, 20, 30];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 7;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 7;
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header'] = (function () {
                            return {
                                columns: [{
                                        alignment: 'left',
                                        italics: true,
                                        text: 'Interior Design',
                                        fontSize: 18,
                                        margin: [10, 0]
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 14,
                                        text: 'All Products'
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages
                        doc['footer'] = (function (page, pages) {
                            return {
                                columns: [{
                                        alignment: 'left',
                                        text: ['Date: ', {
                                            text: jsDate.toString()
                                        }]
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['page ', {
                                            text: page.toString()
                                        }, ' of ', {
                                            text: pages.toString()
                                        }]
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['hLineWidth'] = function (i) {
                            return .5;
                        };
                        objLayout['vLineWidth'] = function (i) {
                            return .5;
                        };
                        objLayout['hLineColor'] = function (i) {
                            return '#aaa';
                        };
                        objLayout['vLineColor'] = function (i) {
                            return '#aaa';
                        };
                        objLayout['paddingLeft'] = function (i) {
                            return 4;
                        };
                        objLayout['paddingRight'] = function (i) {
                            return 4;
                        };
                        doc.content[0].layout = objLayout;
                    }
                },
                'print'
            ],
            ajax: '{{route("product.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "featured_image",
                    "defaultContent": ""
                },
                {
                    "data": "name",
                    "defaultContent": ""
                },
                {
                    "data": "is_featured",
                    "defaultContent": ""
                },
                {
                    "data": "main_view",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": ""
                },
                {
                    "data": "status",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": 1,
                    "render": function (data, type, row, meta) {
                        return `<img src='{{url('')}}/uploads/` + data +
                            `' height='50px' alt='image'/>`;
                    },
                },
                {
                    "targets": 3,
                    "render": function (data, type, row, meta) {
                        if(row.is_featured == 1)
                        {
                            return `<i class="fa fa-check" style="color:green;"></i>`;

                        }
                        else
                        {
                            return 'No';
                        }
                    },

                },
                {
                    "targets": 4,
                    "render": function (data, type, row, meta) {
                        if(row.main_view == 1)
                        {
                            return `<i class="fa fa-check" style="color:green;"></i>`;
                        }
                        else
                        {
                            return 'No';
                        }
                    },
                },
                {
                    "targets": -2,
                    "render": function (data, type, row, meta) {
                       return moment(data).format('MMMM Do YYYY, h:mm a');
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("product.edit",[":id"])}}';
                        edit = edit.replace(':id', row.id);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
          <a href="` + edit + `" class="text-info p-1" data-original-title="Edit" title="" data-placement="top" data-toggle="tooltip" style="padding-right:6px;">
              <i class="fa fa-pencil"></i>
          </a>
          <a href="javascript:;" class="delete text-danger p-2" data-original-title="Delete" title="" data-placement="top" data-toggle="tooltip" data-id="` + row.id + `" style="padding-right:6px;">
              <i class="fa fa-trash-o"></i>
          </a>
          <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                            checked + ` value="` + row.id + `">
          `;
                    },
                },
            ],
            "drawCallback": function (settings) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                if (elems) {
                    elems.forEach(function (html) {
                        var switchery = new Switchery(html, {
                            color: '#36c6d3',
                            secondaryColor: '#dfdfdf',
                            jackColor: '#fff',
                            jackSecondaryColor: null,
                            className: 'switchery',
                            disabled: false,
                            disabledOpacity: 0.5,
                            speed: '0.1s',
                            size: 'small'
                        });

                    });
                }

                $('.status').change(function () {
                    var $this = $(this);
                    var id = $this.val();
                    var status = this.checked;

                    if (status) {
                        status = 1;
                    } else {
                        status = 0;
                    }

                    axios
                        .post('{{route("product.status")}}', {
                            _token: '{{csrf_token()}}',
                            _method: 'patch',
                            id: id,
                            status: status,
                        })
                        .then(function (responsive) {
                            console.log(responsive);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                });

                $('.delete').click(function () {
                    var deleteId = $(this).data('id');
                    var $this = $(this);

                    Swal.fire ({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        icon: 'warning',

                        showCancelButton: true,
                        confirmButtonColor: '#4fa7f3',
                        cancelButtonColor: '#d57171',
                        confirmButtonText: 'Yes, delete it!'
                    })
                       .then(function (result) {
                        if (result.value) {
                            axios
                                .post('{{route("product.delete")}}', {
                                    _method: 'delete',
                                    _token: '{{csrf_token()}}',
                                    id: deleteId,
                                })
                                .then(function (response) {
                                    console.log(response);

                                    Swal.fire(
                                        'Deleted!',
                                        'Product has been deleted.',
                                        'success'
                                    )

                                    table
                                        .row($this.parents('tr'))
                                        .remove()
                                        .draw();
                                })
                                .catch(function (error) {
                                    console.log(error);
                                    Swal.fire(
                                        'Failed!',
                                        error.response.data.error,
                                        'error'
                                    )
                                });
                        }
                    })
                });
            },
            //scrollX:true,
        });
        $('#advanceSearch').submit(function (e) {
            e.preventDefault();
            table.columns(2).search($('input[name="name"]').val());
            table.columns(3).search($('.featured').val());
            table.columns(6).search($('.ver-status').val());
            table.draw();
        });

        $(".custom_datatable #DataTable_wrapper .row:nth-child(2) .col-sm-12").niceScroll();
    });
</script>
@endsection