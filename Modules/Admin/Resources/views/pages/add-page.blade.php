@extends('admin::layouts.master')
@section('title', 'Add Page')

@section('top-styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
    rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
    type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet"
    type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }
</style>
@endsection
@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('dashboard')}}">ADMIN</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{route('pages')}}">Pages</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Add Page</span>
    </li>
</ul>
@endsection

<!-- BEGIN Slider STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-file"> </i> Add Page
                        <button onclick="window.history.back(1)" type="button"
                            class="btn btn-block btn-primary btn-md float-sm-right">Go
                            Back</button>
                    </div>
                    <div class="panel-body">
                        <!-- form start -->
                        <!-- form start -->
                        <form action="{{$isEdit ? route('page.update',$page->id) : route('page.store')}}" role="form"
                            method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                            <input type="hidden" name="_method" value="put">
                            @endif
                            <input type="hidden" name="user_type" value="1">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="heading">Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter Name" value="{{$page->name ?? old('name')}}">
                                        <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="heading">Sort By <span class="text-danger">*</span></label>
                                        <input type="number" name="sort_by" class="form-control" id="sort_by"
                                            placeholder="Sort By" value="{{$page->sort_by ?? old('sort_by')}}">
                                        <span class="text-danger">{{$errors->first('sort_by') ?? null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary button-custom">{{$isEdit ? 'Update' : 'Save'}}</button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END Page -->
@section('page-scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- jquery-validation -->
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/additional-methods.min.js"></script>

<script src="{{url('')}}/dash-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/additional-methods.min.js"
    type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
    type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#quickForm').validate({
            rules: {
                heading: {
                    required: true,
                },
                button_text: {
                    required: true,
                },
                button_link: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');

            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection
@endsection