
<script type="text/javascript">

  $(document).ready(function () {

    // updates the home header in the database via AJAX
    $('#project-section').click(function(){
      var formData = new FormData();
      formData.append("_token", $('#token').val());


      formData.append("heading", $('.project-heading').val());
      axios.post('{{route("home-project.update",$page_id)}}', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function(res){
        Swal.fire(
          'Updated!',
          'Project Section updated Sucessfully.',
          'success'
        )
      }).catch(function(err){
        Swal.fire(
          'Failed!',
          err.message,
          'error'
        )
        console.log(err);
      });
    });
  });
</script>
