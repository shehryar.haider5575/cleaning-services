
<script type="text/javascript">

  $(document).ready(function () {

    // updates the home header in the database via AJAX
    $('#work-section').click(function(){
      var formData = new FormData();
      formData.append("_token", $('#token').val());


      formData.append("heading", $('.work-heading').val());
      formData.append("text", $('.work > .note-editor > .note-editing-area > .note-editable').html());
      axios.post('{{route("home-work.update",$page_id)}}', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function(res){
        Swal.fire(
          'Updated!',
          'Years of Work Section updated Sucessfully.',
          'success'
        )
      }).catch(function(err){
        Swal.fire(
          'Failed!',
          err.message,
          'error'
        )
        console.log(err);
      });
    });
  });
</script>
