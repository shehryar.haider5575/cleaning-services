
<script type="text/javascript">

  $(document).ready(function () {

    // updates the home header in the database via AJAX
    $('#contact-section').click(function(){
      var formData = new FormData();
      formData.append("_token", $('#token').val());


      formData.append("heading", $('.contact-heading').val());
      formData.append("text", $('.contact > .note-editor > .note-editing-area > .note-editable').html());
      axios.post('{{route("home-contact.update",$page_id)}}', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function(res){
        Swal.fire(
          'Updated!',
          'Contact Section updated Sucessfully.',
          'success'
        )
      }).catch(function(err){
        Swal.fire(
          'Failed!',
          err.message,
          'error'
        )
        console.log(err);
      });
    });
  });
</script>
