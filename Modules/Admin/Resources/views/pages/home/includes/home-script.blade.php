
<script type="text/javascript">

  $(document).ready(function () {

    // updates the home header in the database via AJAX
    $('#about-section').click(function(){
      var formData = new FormData();
      formData.append("_token", $('#token').val());


      formData.append("heading", $('.main-heading').val());
      formData.append("sub_heading_1", $('.below-heading').val());
      formData.append("sub_heading_2", $('.sub-heading').val());

      formData.append("text", $('.text').val());

      var attachment = document.querySelector('#imageUpload1');
      if(attachment.files[0]){
        formData.append('attachment',attachment.files[0]);
      }

      axios.post('{{route("home-about.update",$page_id)}}', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function(res){
        Swal.fire(
          'Updated!',
          'About Section updated Sucessfully.',
          'success'
        )
      }).catch(function(err){
        Swal.fire(
          'Failed!',
          err.message,
          'error'
        )
        console.log(err);
      });
    });
  });
</script>
