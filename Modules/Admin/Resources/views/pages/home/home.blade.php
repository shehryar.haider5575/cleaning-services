@extends('admin::layouts.master')
@section('title', 'Home Page')

@section('top-styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
  rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
  type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet"
  type="text/css" />
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">

<!-- Plugins css-->
<link href="{{url('')}}/dash-assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
  rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->
<style>
  a.btn.btn-secondary {
    background: #dc3535bf;
    border: 1px solid white;
  }

  h2#swal2-title {
    font-size: 25px;
  }

  div#swal2-content {
    font-size: 17px;
  }

  .swal2-icon.swal2-warning,
  .swal2-success-ring {
    border-radius: 50% !important;
    font-size: 11px;
  }

  .swal2-popup.swal2-modal.swal2-icon-warning.swal2-show {
    height: 280px;
    width: 36em;
  }

  table.dataTable.no-footer {
    border-bottom: inherit !important;
  }
  span.switchery.switchery-small {
    margin: 11px;
}
</style>
@endsection
@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
  <li>
    <a href="{{route('dashboard')}}">ADMIN</a>
    <i class="fa fa-circle"></i>
  </li>
  <li>
    <span>Home Page</span>
  </li>
</ul>
@endsection

<div class="row">
  <div class="col-md-12">
    <!-- jquery validation -->
    <div class="card">
      <!-- /.card-header -->
      <div class="card-body">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <i class="fa fa-home"> </i> Home Page
            <button onclick="window.history.back(1)" type="button"
              class="btn btn-block btn-primary btn-md float-sm-right">Go
              Back</button>
          </div>
          <div class="panel-body">
            @php
            $about_section = $page->where('type',1)->first();
            @endphp
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box red" style="border:1px solid #36c6d3;">
              <div class="portlet-title" style="background-color: #219da9">
                <div class="caption">
                  About  Section</div>
                  <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" data-status="{{$about_section->status == 1 ? '1' : '0'}}" {{$about_section->status == 1 ? 'checked' : null}} data-type="1">
                  <div class="tools">
                  <a href="javascript:;" class="collapse"> </a>
                </div>
              </div>
              <div class="portlet-body" style="display: none" id="section">
                <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                <div class="row">
                  <div class="col-md-6">
                    <div class="avatar-upload" style="height: 200px;">
                      <div class="avatar-edit">
                        <input type='file' name="image" id="imageUpload1" accept=".png, .jpg, .jpeg" />
                        <label for="imageUpload1"><span>Featured Image </span></label>
                      </div>
                      <div class="avatar-preview">
                        <div id="imagePreview1"
                          style="background-image : url({{url('').'/uploads/'}}{{!empty($about_section->attachment) ? $about_section->attachment : 'placeholder.jpg'}}">
                        </div>
                      </div>
                    </div>
                    <span class="text-danger">{{$errors->first('image') ?? null}}</span>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Main Heading
                        <span class="text-danger">*</span>
                      </label>
                      <input type="text" name="heading" parsley-trigger="change" required placeholder="Heading..."
                        class="main-heading form-control" value="{{$about_section->heading ?? null}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Sub Heading 1
                        <span class="text-danger">*</span>
                      </label>
                      <input type="text" name="heading" parsley-trigger="change" required placeholder="Sub Heading 1..."
                        class="below-heading form-control" value="{{$about_section->sub_heading_1 ?? null}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Sub Heading 2
                        <span class="text-danger">*</span>
                      </label>
                      <input type="text" name="heading" parsley-trigger="change" required placeholder="Sub Heading 2..."
                        class="sub-heading form-control" value="{{$about_section->sub_heading_2 ?? null}}">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Text
                        <span class="text-danger">*</span>
                      </label>
                      <textarea type="text" rows="7" name="text" parsley-trigger="change" required
                        class="text form-control summernote">{{$about_section->text ?? null}}</textarea>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button id="about-section" type="button"
                      class="btn btn-block btn-primary btn-md button-custom">Update</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- END Portlet PORTLET-->
              <!-- BEGIN Portlet PORTLET-->
              <div class="portlet box red" style="border:1px solid #36c6d3;">
                <div class="portlet-title" style="background-color: #219da9">
                  @php
                  $project_section = $page->where('type',3)->first();
                  @endphp
                  <div class="caption">
                    Services Section</div>
                    <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" data-status="{{$project_section->status == 1 ? '1' : '0'}}" {{$project_section->status == 1 ? 'checked' : null}} data-type="3">
                  <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                  </div>
                </div>
                <div class="portlet-body" style="display: none" id="section">
                  <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Main Heading
                          <span class="text-danger">*</span>
                        </label>
                        <input type="text" name="heading" parsley-trigger="change" required placeholder="Heading..."
                          class="project-heading form-control" value="{{$project_section->heading ?? null}}">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <button id="project-section" type="button"
                        class="btn btn-block btn-primary btn-md button-custom">Update</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END Portlet PORTLET-->
              <!-- BEGIN Portlet PORTLET-->
               <div class="portlet box red" style="border:1px solid #36c6d3;">
                <div class="portlet-title" style="background-color: #219da9">
                  @php
                  $client_section = $page->where('type',4)->first();
                  @endphp
                  <div class="caption">
                    Client Section</div>
                    <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" data-status="{{$client_section->status == 1 ? '1' : '0'}}" {{$client_section->status == 1 ? 'checked' : null}} data-type="4">
                  <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                  </div>
                </div>
                <div class="portlet-body" style="display: none" id="section">
                  <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Main Heading
                          <span class="text-danger">*</span>
                        </label>
                        <input type="text" name="heading" parsley-trigger="change" required placeholder="Heading..."
                          class="client-heading form-control" value="{{$client_section->heading ?? null}}">
                      </div>
                    </div>
                    {{-- <div class="col-md-12">
                      <div class="form-group client">
                        <div name="summernote" id="summernote_1" class="summernote_2 summernote_1">{{$client_section->text ?? null}} </div>
                      </div>
                    </div> --}}
                    <div class="col-md-2">
                      <button id="client-section" type="button" class="btn btn-block btn-primary btn-md button-custom">Update</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END Portlet PORTLET-->
               <!-- BEGIN Portlet PORTLET-->
               <div class="portlet box red" style="border:1px solid #36c6d3;">
                <div class="portlet-title" style="background-color: #219da9">
                  @php
                  $work_section = $page->where('type',2)->first();
                @endphp
                  <div class="caption">
                    Years of Working</div>
                    <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" data-status="{{$work_section->status == 1 ? '1' : '0'}}" {{$work_section->status == 1 ? 'checked' : null}} data-type="2">
                  <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                  </div>
                </div>
                <div class="portlet-body" style="display: none" id="section">
                  <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Main Heading
                          <span class="text-danger">*</span>
                        </label>
                        <input type="text" name="heading" parsley-trigger="change" required placeholder="Heading..."
                          class="work-heading form-control" value="{{$work_section->heading ?? null}}">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group work">
                        <div name="summernote" id="summernote_1" class="summernote_2 summernote_1">{{$work_section->text ?? null}} </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <button id="work-section" type="button" class="btn btn-block btn-primary btn-md button-custom">Update</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END Portlet PORTLET-->
              <!-- BEGIN Portlet PORTLET-->
              <div class="portlet box red" style="border:1px solid #36c6d3;">
                <div class="portlet-title" style="background-color: #219da9">
                  @php
                  $contact_section = $page->where('type',5)->first();
                  @endphp
                  <div class="caption">
                    Contact Section
                  </div>
                  <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" data-status="{{$contact_section->status == 1 ? '1' : '0'}}" {{$contact_section->status == 1 ? 'checked' : null}} data-type="5">
                    <div class="tools">
                      <a href="javascript:;" class="collapse"> </a>
                  </div>
                </div>
                <div class="portlet-body" style="display: none" id="section">
                  <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Main Heading
                          <span class="text-danger">*</span>
                        </label>
                        <input type="text" name="heading" parsley-trigger="change" required placeholder="Heading..."
                          class="contact-heading form-control" value="{{$contact_section->heading ?? null}}">
                      </div>
                    </div>
                    {{-- <div class="col-md-12">
                      <div class="form-group contact">
                        <div name="summernote" class="summernote_2 summer1">{{$contact_section->text ?? null}} </div>
                      </div>
                    </div> --}}
                    <div class="col-md-2">
                      <button id="contact-section" type="button" class="btn btn-block btn-primary btn-md button-custom">Update</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END Portlet PORTLET-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- END Page -->
    @endsection
    @section('page-scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- jquery-validation -->
    <script src="{{url('')}}/dash-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{url('')}}/dash-assets/plugins/jquery-validation/additional-methods.min.js"></script>

    <script src="{{url('')}}/dash-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
      type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/additional-methods.min.js"
      type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
      type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"
      type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
      type="text/javascript">
    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"
      type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"
      type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript">
    </script>
    <script src="{{url('')}}/dash-assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    {{-- SweetAlert2 --}}
    <script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

    <script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
    <script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
      type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript">
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- END PAGE LEVEL PLUGINS -->
    @endsection
    @section('custom-script')
    <script type="text/javascript">
      $(document).ready(function () {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
          if (elems) {
              elems.forEach(function (html) {
                  var switchery = new Switchery(html, {
                      color: '#36c6d3',
                      secondaryColor: '#dfdfdf',
                      jackColor: '#fff',
                      jackSecondaryColor: null,
                      className: 'switchery',
                      disabled: false,
                      disabledOpacity: 0.5,
                      speed: '0.1s',
                      size: 'small'
                  });

              });
          }
        
          $('.status').change(function () {
            var $this = $(this);
            var type = $this.data('type');
            var status = $this.data('status');
            console.log('type'+ type + 'status' + status);
            axios
                .post('{{route("home.page.status",$page_id)}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'patch',
                    type: type,
                    status: status,
                })
                .then(function (responsive) {
                    console.log(responsive);
                })
                .catch(function (error) {
                    console.log(error);
                });
        });


        function readURL(input, number) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
              $('#imagePreview' + number).hide();
              $('#imagePreview' + number).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
          }
        }
        $("#imageUpload1").change(function () {
          readURL(this, 1);
        });
      });
    </script>
    @include('admin::pages.home.includes.home-script')
    @include('admin::pages.home.includes.project-script')
    @include('admin::pages.home.includes.client-script')
    @include('admin::pages.home.includes.work-script')
    @include('admin::pages.home.includes.contact-script')

    @endsection