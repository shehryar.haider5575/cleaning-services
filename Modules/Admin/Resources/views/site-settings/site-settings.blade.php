@extends('admin::layouts.master')
@section('title', 'Site Setting')

@section('top-styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
    rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
    type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet"
    type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<style>
    .images {
        height: 150px;
        border: 1px solid #8080807d;
        margin: 11px;
    }

    span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }
</style>
@endsection
@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('dashboard')}}">ADMIN</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Site Setting</span>
    </li>
</ul>
@endsection

<!-- BEGIN Products STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-cogs"> </i> Site Setting
                        <button onclick="window.history.back(1)" type="button"
                            class="btn btn-block btn-primary btn-md float-sm-right"
                            >Go
                            Back</button>
                    </div>
                    <div class="panel-body">
                        <!-- form start -->
                        <!-- BEGIN FORM-->
                        <!-- form start -->
                        {{-- <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button> --}}
                        <form action="{{route('site.setting.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="avatar-upload" style="height: 200px;">
                                        <div class="avatar-edit">
                                            <input type='file' name="logo" id="imageUpload1" accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Company Logo</span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/'}}{{$setting->where('key','logo')->first() ? $setting->where('key','logo')->first()->value : 'placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8" style="margin-top: 5px">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" name="name" parsley-trigger="change" required
                                            placeholder="Company Name..." class="form-control" id="phone-1"
                                            value="{{$setting->where('key','name')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone 1</label>
                                        <input type="text" name="phone-1" parsley-trigger="change" required
                                            placeholder="Phone 1..." class="form-control" id="phone-1"
                                            value="{{$setting->where('key','phone-1')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone 2</label>
                                        <input type="text" name="phone-2" parsley-trigger="change" required=""
                                            placeholder="Phone 2..." class="form-control" value="{{$setting->where('key','phone-2')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" parsley-trigger="change" required=""
                                            placeholder="Email..." class="form-control" value="{{$setting->where('key','email')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Copyrights &copy;</label>
                                        <input type="text" name="copyrights" parsley-trigger="change" required=""
                                            placeholder="Copyrights..." class="form-control" value="{{$setting->where('key','copyrights')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-11" style="margin-top: 10px">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" name="address" parsley-trigger="change" required=""
                                            placeholder="Address..." class="form-control" value="{{$setting->where('key','address')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <h4 class="col-md-11" style="font-weight: bold;">Socail Links:</h4>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <input type="text" name="facebook" parsley-trigger="change" required
                                            placeholder="Facebook..." class="form-control" id="facebook"
                                            value="{{$setting->where('key','facebook')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <input type="text" name="twitter" parsley-trigger="change" required=""
                                            placeholder="Twitter..." class="form-control" value="{{$setting->where('key','twitter')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Linkedin</label>
                                        <input type="text" name="linkedin" parsley-trigger="change" required
                                            placeholder="Linkedin..." class="form-control" id="linkedin"
                                            value="{{$setting->where('key','linkedin')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Instagram</label>
                                        <input type="text" name="instagram" parsley-trigger="change" required=""
                                            placeholder="Instagram..." class="form-control" value="{{$setting->where('key','instagram')->first()->value ?? null}}">
                                    </div>
                                </div>
                                <div class="col-md-11">   
                                    <button type="submit" class="btn btn-block btn-primary btn-md float-sm-right" >Update</button>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- END Page -->
@section('page-scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- jquery-validation -->
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/additional-methods.min.js"></script>

<script src="{{url('')}}/dash-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/additional-methods.min.js"
    type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
    type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#quickForm').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    emai: true,
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');

            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });
    });
</script>
@endsection
@endsection