@extends('admin::layouts.master')
@section('title', 'Add Specialization')

@section('top-styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
  rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
  type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
  type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet"
  type="text/css" />
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">

<!-- Plugins css-->
<link href="{{url('')}}/dash-assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
  rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->
<style>
  a.btn.btn-secondary {
    background: #dc3535bf;
    border: 1px solid white;
  }

  h2#swal2-title {
    font-size: 25px;
  }

  div#swal2-content {
    font-size: 17px;
  }

  .swal2-icon.swal2-warning,
  .swal2-success-ring {
    border-radius: 50% !important;
    font-size: 11px;
  }

  .swal2-popup.swal2-modal.swal2-icon-warning.swal2-show {
    height: 280px;
    width: 36em;
  }

  table.dataTable.no-footer {
    border-bottom: inherit !important;
  }
</style>
@endsection
@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
  <li>
    <a href="{{route('dashboard')}}">ADMIN</a>
    <i class="fa fa-circle"></i>
  </li>
  <li>
    <a href="{{route('services')}}">Services</a>
    <i class="fa fa-circle"></i>
  </li>
  <li>
    <span>Add Service</span>
  </li>
</ul>
@endsection

<div class="row">
  <div class="col-md-12">
    <!-- jquery validation -->
    <div class="card">
      <!-- /.card-header -->
      <div class="card-body">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <i class="fa fa-cogs"> </i> Add Service
            <button onclick="window.history.back(1)" type="button"
              class="btn btn-block btn-primary btn-md float-sm-right">Go
              Back</button>
          </div>
          <div class="panel-body">
            <form action="{{$isEdit ? route('service.update',$service->id) : route('service.store')}}" role="form"
              method="POST" id="quickForm" enctype="multipart/form-data">
              @csrf
              @if ($isEdit)
              <input type="hidden" name="_method" value="put">
              @endif
              <input type="hidden" name="user_type" value="1">
              <div class="row">
                <div class="col-md-4">
                  <div class="avatar-upload" style="height: 225px;">
                    <div class="avatar-edit">
                      <input type='file' name="attachment" id="imageUpload1" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload1"><span style="background: none;">Logo</span></label>
                    </div>
                    <div class="avatar-preview">
                      <div id="imagePreview1"
                        style="background-image : url({{url('').'/uploads/'}}{{$isEdit ? $service->attachment : 'placeholder.jpg'}}">
                      </div>
                    </div>
                  </div>
                  <span class="text-danger">{{$errors->first('attachment') ?? null}}</span>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="name">Name <span class="text-danger">*</span></label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name"
                      value="{{$isEdit ? $service->name : old('name')}}">
                    <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                  </div>
                </div>
                <div class="col-md-8">
                  {{-- <br> --}}
                  <div class="form-group">
                    <label for="heading">Description</label>
                    <textarea name="description" class="form-control" rows="6">
                      {{$service->description ?? old('description')}}
                    </textarea>
                    <span class="text-danger">{{$errors->first('description') ?? null}}</span>
                  </div>
                </div>
              </div>
              <br>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary button-custom">{{$isEdit ? 'Update' : 'Save'}}</button>
              </div>
            </form>
            <!-- END FORM-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END Page -->

@endsection
@section('page-scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- jquery-validation -->
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/jquery-validation/additional-methods.min.js"></script>

<script src="{{url('')}}/dash-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/additional-methods.min.js"
  type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
  type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript">
</script>
<script src="{{url('')}}/dash-assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

<script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
  type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('custom-script')
<script type="text/javascript">
  $(document).ready(function () {

    function readURL(input, number) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
          $('#imagePreview' + number).hide();
          $('#imagePreview' + number).fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#imageUpload1").change(function () {
      readURL(this, 1);
    });
  });
</script>

@endsection