<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Page;
use App\Models\ContentPage;
use DataTables;
use Storage;

class ContentPageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::pages.content.pages');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $pages = Page::whereNull('deleted_at')->where('status',1)->orderBy('sort_by','asc')->select(['id','name']);
        return DataTables::of($pages)->make();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function page(Page $page)
    {
        $data = [
            'page'      =>  $page->contentPages,
            'page_id'   =>  $page->id,
            'isEdit'    =>  false,
        ];

        switch ($page->name) {
            case 'Home':
                return view('admin::pages.home.home',$data);
                break;
            case 'About Us':
                return view('admin::pages.about.about',$data);
                break;
        }
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function homeAboutUpdate(Request $request, $page)
    {
        $cms = ContentPage::where([['page_id',$page],['type',1]])->first();
        $data = $request->except('_token');
        $data['page_id']    =  $page;
        $data['type']       =  1;
        if(empty($cms))
        {
            $cms = new ContentPage;
            $data['attachment'] = Storage::disk('uploads')->putFile('',$request->attachment);
            $cms->create($data);
            $response['status'] = true;
            $response['message'] = 'About Section updated successfully.';
            return response()->json($response, 200);
        }
        if($request->attachment)
        {
            Storage::disk('uploads')->delete($cms->attachment);
            $data['attachment'] = Storage::disk('uploads')->putFile('',$request->attachment);
        }
        $cms->update($data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function homeProjectUpdate(Request $request, $page)
    {
        $cms = ContentPage::where([['page_id',$page],['type',3]])->first();
        $data = $request->except('_token');
        $data['page_id']    =  $page;
        $data['type']       =  3;

        if(empty($cms))
        {
            $cms = new ContentPage;
            $cms->create($data);
            $response['status'] = true;
            $response['message'] = 'Project Section updated successfully.';
            return response()->json($response, 200);
        }
        
        $cms->update($data);
        $response['status'] = true;
        $response['message'] = 'Project Section updated successfully.';
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function homeClientUpdate(Request $request, $page)
    {
        $cms = ContentPage::where([['page_id',$page],['type',4]])->first();
        $data = $request->except('_token');
        $data['page_id']    =  $page;
        $data['type']       =  4;
        if(empty($cms))
        {
            $cms = new ContentPage;
            $cms->create($data);
            $response['status'] = true;
            $response['message'] = 'Client Section updated successfully.';
            return response()->json($response, 200);
        }
        $cms->update($data);
        $response['status'] = true;
        $response['message'] = 'Client Section updated successfully.';
        return response()->json($response, 200);
    }

     /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function homeWorkUpdate(Request $request, $page)
    {
        $cms = ContentPage::where([['page_id',$page],['type',2]])->first();
        $data = $request->except('_token');
        $data['page_id']    =  $page;
        $data['type']       =  2;
        if(empty($cms))
        {
            $cms = new ContentPage;
            $cms->create($data);
            $response['status'] = true;
            $response['message'] = 'Work Section updated successfully.';
            return response()->json($response, 200);
        }
        $cms->update($data);
        $response['status'] = true;
        $response['message'] = 'Work Section updated successfully.';
        return response()->json($response, 200);
    }

     /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function homeContactUpdate(Request $request, $page)
    {
        $cms = ContentPage::where([['page_id',$page],['type',5]])->first();
        $data = $request->except('_token');
        $data['page_id']    =  $page;
        $data['type']       =  5;
        if(empty($cms))
        {
            $cms = new ContentPage;
            $cms->create($data);
            $response['status'] = true;
            $response['message'] = 'Contact Section updated successfully.';
            return response()->json($response, 200);
        }
        $cms->update($data);
        $response['status'] = true;
        $response['message'] = 'Contact Section updated successfully.';
        return response()->json($response, 200);
    }

    /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function homePageStatus(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $type = $request->input('type');
        $status = $request->input('status') == 1 ? 0 : 1;

        $item = ContentPage::where('type',$type)->first();

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

}
