<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Product;
use Auth;
use Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'total_products'    =>  Product::count(),
        ];
        return view('admin::dashboard',$data);
    }

    /**
     * Terminate sesssion of specified user.
     * @return Renderable
     */
    public function logout()
    {
        if(Auth::check())
        {
            Auth::logout();
            return redirect('admin/login');
        }
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function profile()
    {
        $data = [
            'user'    =>  Auth::user(),
        ];
        return view('admin::profile.setting',$data);
    }

    public function setting(Request $request)
    {
        $request->validate([
            'name'              => 'required|string|max:100',
            'email'             => "required|max:255|unique:users,email,".Auth::user()->id,
            'current_password'  => 'required_if:change_password,1',
            'password'          => 'required_if:change_password,1|confirmed|min:6|max:22',
            'avatar'            => 'nullable|image|max:3000',
        ]);

        if($request->change_password == 1 && !Hash::check($request->current_password, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['current_password'=>'your current password does not match'])->withInput($request->all());

        }

        $input_data = $request->except('password', 'password_confirmation','_token','_method','change_password','current_password');

        if($request->password)
        {
            $input_data['password'] = bcrypt($request->input('password'));
        }

        if($request->avatar)
        {
            Storage::disk('uploads')->delete(Auth::user()->avatar);
            $input_data['avatar'] = Storage::disk('uploads')->putFile('', $request->avatar);
        }

        Auth::user()->update($input_data);

        return redirect()->route('dashboard');
    }
}
