<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Client;
use DataTables;
use Storage;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::clients.clients');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $clients = Client::whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','company_logo','link','created_at','status']);

        return DataTables::of($clients)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin::clients.add-client',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_logo'      =>  'required|image|max:3000',
            'link'              =>  'required|max:255',
        ]);
        $data = $request->all();
        $data['company_logo'] = Storage::disk('uploads')->putFile('',$request->company_logo);
        // return $data;
        Client::create($data);
        return redirect()->route('clients');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Client $client)
    {
        $data = [
            'isEdit'    =>  true,
            'client'    =>  $client
        ];
        return view('admin::clients.add-client',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'company_logo'      =>  'nullable|image|max:3000',
            'link'              =>  'required|max:255',
        ]);
        $data = $request->all();
      
        if($request->company_logo)
        {
            Storage::disk('uploads')->delete($client->company_logo);
            $data['company_logo'] = Storage::disk('uploads')->putFile('',$request->company_logo);
        }
        $client->update($data);
        return redirect()->route('clients');
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Client::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Client status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $client = Client::findOrFail($request->id);
        // apply your conditional check here
        if ( $client->delete() ) {
            $response['success'] = 'Data Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($client->image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        }
    }
}
