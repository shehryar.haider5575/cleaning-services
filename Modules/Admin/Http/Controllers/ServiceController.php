<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Service;
use DataTables;
use Storage;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::services.services');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $sliders = Service::whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','attachment','name','created_at','status']);

        return DataTables::of($sliders)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin::services.add-service',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'attachment'    =>  'required|mimes:png,jpg,jpeg,webp|max:3000',
            'name'          =>  'required|max:100', 
        ]);
        $data = $request->all();
        if($request->attachment)
        {
            $data['attachment'] = Storage::disk('uploads')->putFile('',$request->attachment);
        }
        // return $data;
        Service::create($data);
        return redirect()->route('services');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Service $service)
    {
        $data = [
            'isEdit'       =>  true,
            'service'      =>  $service
        ];
        return view('admin::services.add-service',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request,Service $service)
    {
        $request->validate([
            'attachment'    =>  'nullable|mimes:png,jpg,jpeg,webp|max:3000',
            'name'          =>  'required|max:100', 
        ]);
        $data = $request->all();
      
        if($request->attachment)
        {
            Storage::disk('uploads')->delete($service->attachment);
            $data['attachment'] = Storage::disk('uploads')->putFile('',$request->attachment);
        }
        $service->update($data);
        return redirect()->route('services');
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Service::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $service = Service::findOrFail($request->id);
        // apply your conditional check here
        if ($service->delete()) {
            $response['success'] = 'Service Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($service->image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        }
    }
}
