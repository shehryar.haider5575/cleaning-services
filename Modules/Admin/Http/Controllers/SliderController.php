<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Slider;
use DataTables;
use Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::sliders.sliders');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $sliders = Slider::whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','image','heading','description','button_text','button_link','created_at','status']);

        return DataTables::of($sliders)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin::sliders.add-slider',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'heading'           =>  'required|max:100',
            'description'       =>  'required|max:255',
            'image'             =>  'required|mimes:png,jpg,jpeg,webp|max:3000',
            'button_text'       =>  'required|max:10',
            'button_link'       =>  'required|max:255',
        ]);
        $data = $request->all();
        $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        // return $data;
        Slider::create($data);
        return redirect()->route('sliders');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Slider $slider)
    {
        $data = [
            'isEdit'    =>  true,
            'slider'    =>  $slider
        ];
        return view('admin::sliders.add-slider',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'heading'           =>  'required|max:100',
            'description'       =>  'required|max:255',
            'image'             =>  'nullable|mimes:png,jpg,jpeg,webp|max:3000',
            'button_text'       =>  'required|max:10',
            'button_link'       =>  'required|max:255',
        ]);
        $data = $request->all();
        if($request->image)
        {
            Storage::disk('uploads')->delete($slider->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $slider->update($data);
        return redirect()->route('sliders');
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Slider::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Slider status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $slider = Slider::findOrFail($request->id);
        // apply your conditional check here
        if ($slider->delete()) {
            $response['success'] = 'Data Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($slider->image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        }
    }
}
