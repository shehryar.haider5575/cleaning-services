<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Category;
use DataTables;
use Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::categories.category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $category = Category::whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','name','created_at','status']);

        return DataTables::of($category)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin::categories.add-category',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          =>  'required|max:100|unique:categories,name',
            'image'         =>  'nullable|mimes:png,jpg,jpeg,webp|max:3000',
            'description'   =>  'nullable|max:3000'
        ]);
        $data = $request->all();
        if($request->image)
        {
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        // return $data;
        Category::create($data);
        return redirect()->route('categories');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Category $category)
    {
        $data = [
            'isEdit'        =>  true,
            'category'      =>  $category
        ];
        return view('admin::categories.add-category',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name'          =>  'required|max:100|unique:categories,name,'.$category->id,
            'image'         =>  'nullable|mimes:png,jpg,jpeg,webp|max:3000',
            'description'   =>  'nullable|max:3000'
        ]);
        $data = $request->all();
        if($request->image)
        {
            if(!empty($category->image))
            {
                Storage::disk('uploads')->delete($category->image);
            }
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
        }
        $category->update($data);
        return redirect()->route('categories');
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Category::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $category = Category::findOrFail($request->id);
        // apply your conditional check here
        if ($category->delete()) {
            $response['success'] = 'Data Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($category->image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        }
    }
}
