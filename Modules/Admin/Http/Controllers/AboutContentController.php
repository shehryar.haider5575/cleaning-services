<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\ContentPage;
use DataTables;
use Storage;

class AboutContentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($page)
    {
        $data = [
            'page_id' =>    $page,
        ];
        return view('admin::about.about',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable($page)
    {
        $about = ContentPage::whereNull('deleted_at')->where('page_id',$page)->orderBy('created_at','desc')->select(['id','attachment','text','created_at','status']);

        return DataTables::of($about)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($page)
    {
        $data = [
            'isEdit'    =>  false,
            'page_id'   =>  $page
        ];
        return view('admin::pages.about.add-about',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request,$page)
    {
        $request->validate([
            'attachment'        =>  'required|mimes:png,jpg,jpeg,webp|max:3000',
            'text'              =>  'required',
        ]);
        $data = $request->all();
        $data['attachment'] = Storage::disk('uploads')->putFile('',$request->attachment);
        $data['page_id'] = $page;
        $data['type'] = 6;

        // return $data;
        ContentPage::create($data);
        return redirect()->route('redirect.page',$page);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($page, ContentPage $about)
    {
        $data = [
            'isEdit'    =>  true,
            'page_id'   =>  $page,
            'about'     =>  $about
        ];
        return view('admin::pages.about.add-about',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $page, ContentPage $about)
    {
        $request->validate([
            'attachment'        =>  'nullable|mimes:png,jpg,jpeg,webp|max:3000',
            'text'              =>  'required',
        ]);
        $data = $request->all();
        if($request->attachment)
        {
            Storage::disk('uploads')->delete($about->attachment);
            $data['attachment'] = Storage::disk('uploads')->putFile('',$request->attachment);
        }
        $about->update($data);
        return redirect()->route('redirect.page',$page);
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request,$page)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = ContentPage::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request,$page)
    {
        $about = ContentPage::findOrFail($request->id);
        // apply your conditional check here
        if ($about->delete()) {
            $response['success'] = 'Data Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($about->image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
        }
    }
}
