<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Product;
use App\Models\ProductDetail;
use DataTables;
use Storage;

class ShoppingProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::shopping-products.shopping-products');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $products = Product::with('productDetails')->whereNull('deleted_at')->orderBy('created_at','desc')->select(['id','featured_image','name','description','is_featured','main_view','created_at','status']);

        return DataTables::of($products)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin::shopping-products.add-product',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'              =>  'required|max:100',
            'featured_image'    =>  'required|mimes:png,jpg,jpeg,webp|max:3000',
            'images'            =>  'nullable|max:3000',
        ]);
        $data = $request->all();
        $data['featured_image'] = Storage::disk('uploads')->putFile('',$request->featured_image);
        // return $data;
        $product = Product::create($data);
        if($request->images)
        {
            foreach($request->images as $sub_image)
            {
                $product_detail             =   new ProductDetail;
                $product_detail->product_id =   $product->id;
                $product_detail->attachment =   Storage::disk('uploads')->putFile('',$sub_image);
                $product_detail->type       =   'image';
                $product_detail->save();
            }
        }
        return redirect()->route('products');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Product $product)
    {
        $data = [
            'isEdit'    =>  true,
            'product'   =>  $product
        ];
        return view('admin::shopping-products.add-product',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name'              =>  'required|max:100',
            'featured_image'    =>  'nullable|mimes:png,jpg,jpeg,webp|max:3000',
            'images'            =>  'nullable|max:3000',
        ]);
        $data       = $request->all();
        $featured   = isset($request->is_featured) ? $request->is_featured : 0;
        $main_view  = isset($request->main_view) ? $request->main_view : 0;

        if($request->featured_image)
        {
            Storage::disk('uploads')->delete($product->featured_image);
            $data['featured_image'] = Storage::disk('uploads')->putFile('',$request->featured_image);
        }
        $product->update([$data,'is_featured'=>$featured,'main_view'=>$main_view]);
        if($request->images)
        {
            foreach($request->images as $sub_image)
            {
                $product_detail                 =   new ProductDetail;
                $product_detail->product_id     =   $product->id;
                $product_detail->attachment     =   Storage::disk('uploads')->putFile('',$sub_image);
                $product_detail->type           =   'image';
                $product_detail->save();
            }
        }
        return redirect()->route('products');
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Product::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Product status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product_detail = ProductDetail::where('product_id',$product->id)->delete();
        // apply your conditional check here
        if ( $product->delete() ) {
            return response()->json($response, 200);
        } else {
            // Storage::disk('uploads')->delete($product->featured_image);
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);;
            // $response = "delete";
        }
    }

         /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyProductImage(ProductDetail $product_detail)
    {
        Storage::disk('uploads')->delete($product_detail->attachment);
        $product_detail->delete();
        return redirect()->back();    
    }
}
