<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Page;
use DataTables;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin::pages.pages');
}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $pages = Page::whereNull('deleted_at')->orderBy('sort_by','asc')->select(['id','name','sort_by','created_at','status']);

        return DataTables::of($pages)->make();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data = [
            'isEdit'    =>  false,
        ];
        return view('admin::pages.add-page',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      =>  'required|max:255',
            'sort_by'   =>  'nullable|numeric',
        ]);
        $data = $request->all();
        // return $data;
        Page::create($data);
        return redirect()->route('pages');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Page $page)
    {
        $data = [
            'isEdit'    =>  true,
            'page'    =>  $page
        ];
        return view('admin::pages.add-page',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Page $page)
    {
        $request->validate([
            'name'      =>  'required|max:255',
            'sort_by'   =>  'nullable|numeric',
        ]);
        $data = $request->all();
        $page->update($data);
        return redirect()->route('pages');
    }

     /**
     * Update Status of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id = $request->input('id');
        $status = $request->input('status');

        $item = Page::find($id);

        if ($item->update(['status' => $status])) {

            $response['status'] = true;
            $response['message'] = 'Page status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


     /**
     * Remove
     *  the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $page = Page::findOrFail($request->id);
        // apply your conditional check here
        if ($page->delete() ) {
            $response['success'] = 'Data Successfully Deleted.';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops! Something went wrong.';
            return response()->json($response, 409);
            // Storage::disk('uploads')->delete($page->image);
        }
    }
}
