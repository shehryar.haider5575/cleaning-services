<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\SiteSetting;
use Storage;
use Hash;

class SiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'setting'    =>  SiteSetting::all(),
        ];
        return view('admin::site-settings.site-settings',$data);
    }

    public function store(Request $request)
    {
        $input = $request->except('_token','logo');
        if ($request->logo) {
            $logo = SiteSetting::where('key','logo')->first();
            if(!empty($logo))
            {
                Storage::disk('uploads')->delete($logo->value);
            }
            $input['logo'] = Storage::disk('uploads')->putFile('', $request->logo);
        }
        foreach ($input as $key => $value) {
            $setting = SiteSetting::where('key',$key)->first();
            if(empty($setting))
            {
                $setting = new SiteSetting;
                $setting->key = $key;   
            }
            $setting->value = $value;
            $setting->save();
        }

        return redirect()->back();
    }
}
