<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function() {

    Route::get('/', function () {
        return redirect('admin/dashboard');
    });
    Route::get('/login', function () {
        return view('admin::login.login');
    })->name('login_view');
    Route::get('/register', function () {
        return view('admin::login.register');
    })->name('register_view');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/dashboard', 'AdminController@index')->name('dashboard');

        // Route::profile
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/','AdminController@profile')->name('profile');
            Route::patch('/setting','AdminController@setting')->name('profile.settings');
        });

        // ROUTE::CATEGORIES
        Route::group(['prefix' => 'category'], function () {
            
            Route::get('/', 'CategoryController@index')->name('categories');
            Route::get('/datatable', 'CategoryController@datatable')->name('category.datatable');
            Route::get('/create', 'CategoryController@create')->name('category.create');
            Route::post('/store', 'CategoryController@store')->name('category.store');
            Route::get('/edit/{category}', 'CategoryController@edit')->name('category.edit');
            Route::put('/update/{category}', 'CategoryController@update')->name('category.update');
            Route::patch('/status', 'CategoryController@status')->name('category.status');
            Route::delete('/delete', 'CategoryController@destroy')->name('category.delete');
        });

        // ROUTE::SHOPPING PRODUCTS
        Route::group(['prefix' => 'shopping/products'], function () {
            
            Route::get('/', 'ShoppingProductController@index')->name('shopping_products');
            Route::get('/datatable', 'ShoppingProductController@datatable')->name('shopping_product.datatable');
            Route::get('/create', 'ShoppingProductController@create')->name('shopping_product.create');
            Route::post('/store', 'ShoppingProductController@store')->name('shopping_product.store');
            Route::get('/edit/{shopping_product}', 'ShoppingProductController@edit')->name('shopping_product.edit');
            Route::put('/update/{shopping_product}', 'ShoppingProductController@update')->name('shopping_product.update');
            Route::patch('/status', 'ShoppingProductController@status')->name('shopping_product.status');
            Route::delete('/delete', 'ShoppingProductController@destroy')->name('shopping_product.delete');
            Route::get('/images/{product_detail}','ShoppingProductController@destroyProductImage')->name('shopping_product_image.delete');
        });

        // ROUTE::PRODUCTS
        Route::group(['prefix' => 'products'], function () {
            
            Route::get('/', 'ProductController@index')->name('products');
            Route::get('/datatable', 'ProductController@datatable')->name('product.datatable');
            Route::get('/create', 'ProductController@create')->name('product.create');
            Route::post('/store', 'ProductController@store')->name('product.store');
            Route::get('/edit/{product}', 'ProductController@edit')->name('product.edit');
            Route::put('/update/{product}', 'ProductController@update')->name('product.update');
            Route::patch('/status', 'ProductController@status')->name('product.status');
            Route::delete('/delete', 'ProductController@destroy')->name('product.delete');
            Route::get('/images/{product_detail}','ProductController@destroyProductImage')->name('product_image.delete');
        });

        // ROUTE::Sliders
        Route::group(['prefix' => 'sliders'], function () {
        
            Route::get('/', 'SliderController@index')->name('sliders');
            Route::get('/datatable', 'SliderController@datatable')->name('slider.datatable');
            Route::get('/create', 'SliderController@create')->name('slider.create');
            Route::post('/store', 'SliderController@store')->name('slider.store');
            Route::get('/edit/{slider}', 'SliderController@edit')->name('slider.edit');
            Route::put('/update/{slider}', 'SliderController@update')->name('slider.update');
            Route::patch('/status', 'SliderController@status')->name('slider.status');
            Route::delete('/delete', 'SliderController@destroy')->name('slider.delete');
        });

        // ROUTE::Clients
        Route::group(['prefix' => 'clients'], function () {
        
            Route::get('/', 'ClientController@index')->name('clients');
            Route::get('/datatable', 'ClientController@datatable')->name('client.datatable');
            Route::get('/create', 'ClientController@create')->name('client.create');
            Route::post('/store', 'ClientController@store')->name('client.store');
            Route::get('/edit/{client}', 'ClientController@edit')->name('client.edit');
            Route::put('/update/{client}', 'ClientController@update')->name('client.update');
            Route::patch('/status', 'ClientController@status')->name('client.status');
            Route::delete('/delete', 'ClientController@destroy')->name('client.delete');
        });

       

         // ROUTE::Pages
         Route::group(['prefix' => 'pages'], function () {
        
            Route::get('/', 'PageController@index')->name('pages');
            Route::get('/datatable', 'PageController@datatable')->name('page.datatable');
            Route::get('/create', 'PageController@create')->name('page.create');
            Route::post('/store', 'PageController@store')->name('page.store');
            Route::get('/edit/{page}', 'PageController@edit')->name('page.edit');
            Route::put('/update/{page}', 'PageController@update')->name('page.update');
            Route::patch('/status', 'PageController@status')->name('page.status');
            Route::delete('/delete', 'PageController@destroy')->name('page.delete');

            // Content
            Route::group(['prefix' => 'content'], function () {
                Route::get('/', 'ContentPageController@index')->name('page.content');
                Route::get('/datatable', 'ContentPageController@datatable')->name('page.content.datatable');
                Route::get('/page/{page}', 'ContentPageController@page')->name('redirect.page');
                
                // Route::HomePage
                Route::patch('/page/{page}/status', 'ContentPageController@homePageStatus')->name('home.page.status');
                Route::post('/page/{page}/update', 'ContentPageController@homeAboutUpdate')->name('home-about.update');
                Route::post('/page/{page}/project/update', 'ContentPageController@homeProjectUpdate')->name('home-project.update');
                Route::post('/page/{page}/client/update', 'ContentPageController@homeClientUpdate')->name('home-client.update');
                Route::post('/page/{page}/work/update', 'ContentPageController@homeWorkUpdate')->name('home-work.update');
                Route::post('/page/{page}/contact/update', 'ContentPageController@homeContactUpdate')->name('home-contact.update');

                Route::group(['prefix' => '{page}/about'], function () {
                    Route::get('datatable', 'AboutContentController@datatable')->name('about.datatable');
                    Route::get('create', 'AboutContentController@create')->name('about.create');
                    Route::post('store', 'AboutContentController@store')->name('about.store');
                    Route::get('edit/{about}', 'AboutContentController@edit')->name('about.edit');
                    Route::put('update/{about}', 'AboutContentController@update')->name('about.update');
                    Route::patch('status', 'AboutContentController@status')->name('about.status');
                    Route::delete('delete', 'AboutContentController@destroy')->name('about.delete');
                });
                
            });
        });

        Route::group(['prefix' => 'site'], function () {
            Route::get('/settings', 'SiteSettingController@index')->name('site.settings');
            Route::post('/settings/store', 'SiteSettingController@store')->name('site.setting.store');
        });
    });
    Route::get('/logout','AdminController@logout')->name('user_logout');
});
