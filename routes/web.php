<?php

use App\Models\City;
use App\Models\States;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('command', function(){
    \Artisan::call("cache:clear");
    \Artisan::call("route:clear");
    \Artisan::call("config:cache");
});

Auth::routes();

Route::get('/', function () {
    return Redirect::route('home', app()->getLocale());
});
Route::middleware(['setLocale'])->prefix('{lang}')->group(function () {

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard','SiteController@dashboard')->name('user-dashboard');
    Route::get('/myorder','SiteController@myorder')->name('user-myorder');
    Route::get('/myqoute','SiteController@myqoute')->name('user-myqoute');
    Route::get('/myappointment','SiteController@myappointment')->name('user-myappointment');

    Route::get('/booking-form/{service}','SiteController@bookingForm')->name('booking_form');
    Route::post('/submitBooking','SiteController@submitBooking')->name('submitBooking');
    
    Route::get('/appointment-form/{service}','SiteController@appointmentForm')->name('appointment_form');
    Route::post('/booking','SiteController@booking')->name('booking');
    
    Route::get('/quote-form/{service}','SiteController@quoteForm')->name('qoute_form');
    Route::post('/quote-booking','SiteController@quoteBooking')->name('quote-booking');

    Route::get('/forgetpassword','SiteController@forgetpassword')->name('forgetpassword');
    Route::post('/changepassword','SiteController@changepassword')->name('change.password');

    Route::get('/service-detail/{service}','SiteController@serviceDetail')->name('service.detail');
    Route::get('/booking-service/{service}','SiteController@orderservice')->name('booking.service');
});


    Route::get('/','SiteController@home')->name('home');
    Route::get('/home','SiteController@home')->name('home');
    Route::get('/about','SiteController@about')->name('about');
    Route::match(['get', 'post'], '/services', 'SiteController@services')->name('services');

    Route::get('/contacts', function () {
        return view('contact');
    })->name('contacts');
    Route::get('/policy', function () {
        return view('policy');
    })->name('policy');
});

Route::get('states/{id}', function ($id) {
    $states = States::where('country_id', $id)->get();
    return response()->json($states, 200);
})->name('states.list');

Route::get('city/{id}', function ($id) {
    $cities = City::where('state_id', $id)->get();
    return response()->json($cities, 200);
})->name('cities.list');

Route::get('/set-lang/{locale}', function ($locale) {
    if (isset($locale)) {
        App::setlocale($locale);
    }
    // dd(app()->getLocale());
    return Redirect::route('home', app()->getLocale());
})->name('set-lang');



