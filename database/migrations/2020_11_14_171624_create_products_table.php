<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('featured_image', 255);
            $table->string('name', 100);
            $table->text('description')->nullable();
            // $table->string('code', 100)->nullable();
            // $table->string('price', 100)->nullable();
            // $table->string('stock', 10)->nullable();
            $table->tinyInteger('is_featured')->default(0)->comment('0 is not featured | 1 is featured');
            $table->tinyInteger('main_view')->default(0)->comment('0 is not show on home view | 1 is show on home view');
            $table->tinyInteger('status')->default(1)->comment('0 is not active | 1 is active');;
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
