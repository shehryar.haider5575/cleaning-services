<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('page_id');
            $table->string('attachment')->nullable();
            $table->string('heading')->nullable();
            $table->string('sub_heading_1')->nullable();
            $table->string('sub_heading_2')->nullable();
            $table->longText('text')->nullable();
            $table->tinyInteger('type');
            $table->tinyInteger('status')->default(1)->comment('0 is not active | 1 is active');
            $table->bigInteger('created_by');
            $table->bigInteger('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms');
    }
}
