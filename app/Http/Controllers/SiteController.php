<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Page;
use App\Models\Quote;
use App\Models\Client;
use App\Models\Country;
use App\Models\Product;
use App\Models\Service;
// use App\Models\ContentPage;
use App\Models\Category;
use App\Models\BookOrder;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Models\ServiceDetail;
use App\User;
use App\Mail\quoteForm;
use \App\Mail\BookForm;
use \App\Mail\AppointmentForm;

class SiteController extends Controller
{
    public function home()
    {
        $services = Service::orderBy('sortorder')->get();
        $countrys = Country::get();
        return view('index', compact('services','countrys'));
    }

    public function services(Request $request)
    {

        $keyword    = $request->has('keyword') ? ($request->keyword != '0' ? $request->keyword : NULL) : NULL;
        $service    = $request->has('service') ? $request->service : NULL;
        $country    = $request->has('country') ? $request->country : NULL;

        $services = ServiceDetail::with('pservice')->when($keyword != null, function ($query) use($keyword){
                return $query->where('service', 'LIKE', '%'.$keyword.'%');
            })->when($service != null, function ($query) use($service){
                return $query->where('categoryid', $service);
            })->when($country != null, function ($query) use($country){
                return $query->where('country_id', $country);
            })
            ->paginate(9);
        // dd($services);
        $service = Service::orderBy('sortorder')->get();
        $countrys = Country::get();
        return view('services', compact('service', 'services','countrys'));
    }

    public function about()
    {
        return view('about');
    }
    
    public function serviceDetail($locale ,Service $service)
    {
        $data = [
            'service'  =>  $service,
            'services' => Service::orderBy('sortorder')->get(),
            'countrys' => Country::get(),
        ];
        return view('cat-services',$data);
    }

    public function orderservice($locale, ServiceDetail $service)
    {
        $data = [
            'service'       =>  $service,
            'appointment'   =>  Appointment::where([['service_id', $service->id],['customer_id', Auth::user()->id]])->first()
        ];
        return view('appointment',$data);
    }
    
    public function bookingForm($locale, ServiceDetail $service)
    {
        $data = [
            'service'       =>  $service,
            'quote'         =>  BookOrder::where([['service_id', $service->id],['customer_id', Auth::user()->id]])->get()
        ];
        return view('bookform',$data);
    }
    
    public function quoteForm($locale, ServiceDetail $service)
    {
        $data = [
            'service'       =>  $service,
            'quote'         =>  Quote::where([['service_id', $service->id],['customer_id', Auth::user()->id]])->get()
        ];
        // dd($data);
        return view('quote',$data);
    }

    public function submitBooking(Request $request)
    {
        $data =  $request->validate([
            'service_id'    =>  'required',
            'address'       =>  'required',
            'contact'       =>  'required',
            'work_time'     =>  'required',
            'rate'          =>  'required',
            'vat'           =>  'required',
            'rate'          =>  'required',
            'comment'       =>  'required'
        ]);
        $data['customer_id']    = Auth::user()->id;
        $data['vendor_id']      = 0;
        $data['heroes']         = 0;
        $data['instruction']    = 0;
        $data['order_status']   = 0;
        $data['entry_date']     = now();
        $BookOrder = BookOrder::whereDate('entry_date',now())->where([['customer_id',Auth::user()->id],['service_id',$data['service_id']]])->first();
        if(empty($BookOrder)){
            $result = BookOrder::create($data);
            $service_name = Service::where('id', $data['service_id'])->first();
            $details = [
                'service_name'  => !empty($service_name) ? $service_name->service : 'No Service',
                'address'       => $request->address,
                'contact'       => $request->contact,
                'work_time'     => $request->work_time,
                'rate'          => $request->rate,
                'comment'       => $request->comment,
            ];
            // \Mail::to('spro.ae@gmail.com')->send(new BookForm($details));
        }
        return view('success-booking');
    }

    public function quoteBooking(Request $request)
    {
        $data =  $request->validate([
            'service_id'    =>  'required',
            'name'          =>  'required',
            'cnumber'       =>  'required',
            'ctime'         =>  'required',
            'email'         =>  'required',
            'comment'       =>  'required',
        ]);
        $data['customer_id']  = Auth::user()->id;
        $data['vendor_id']    = 0;
        $data['entry_date']   = now();
        $quote = Quote::whereDate('entry_date',now())->where([['customer_id',Auth::user()->id],['service_id',$data['service_id']]])->first();
        
        if(empty($quote)){
            $result = Quote::create($data);
            $service_name = Service::find($request->service_id);
            $details = [
                'service_name' => !empty($service_name) ? $service_name->service : 'No Service',
                'name' => $request->name,
                'cnumber' => $request->cnumber,
                'ctime' => $request->ctime,
                'email' => $request->email,
                'comment' => $request->comment,
            ];
            // \Mail::to('spro.ae@gmail.com')->send(new quoteForm($details));
        }
        return view('success-booking');
    }
    
    public function booking(Request $request)
    {
        $data =  $request->validate([
            'service_id'    =>  'required',
            'address'       =>  'required',
            'contact'       =>  'required',
            'adate'         =>  'required',
            'time'          =>  'required',
            'work_time'     =>  'required',
            'rate'          =>  'required',
            'heroes'        =>  'required',
            'instruction'   =>  'required',
        ]);
        $data['customer_id']    = Auth::user()->id;
        $data['vendor_id']    = 0;
        $appointment = Appointment::whereDate('entry_date',now())->where([['customer_id',Auth::user()->id],['service_id',$data['service_id']]])->first();
        
        if(empty($appointment)){
            $last = Appointment::orderBy('id', 'DESC')->first();
            $app_id = explode('SP', $last->app_id);
            $num = 8;
            $new_id = $app_id['1']+1;
            $num = $num - strlen($new_id);
            $app_new_id = 'SP'.str_repeat(0, $num).$new_id;
            $data['app_id'] = $app_new_id;
            $result = Appointment::create($data);
            $service_name = Service::find($request->service_id);
            $details = [
                'service_name' => !empty($service_name) ? $service_name->service : 'No Service',
                'address' => $request->address,
                'contact' => $request->contact,
                'adate' => $request->adate,
                'time' => $request->time,
                'rate' => $request->rate,
                'app_id' => $app_new_id,
            ];
            // \Mail::to('spro.ae@gmail.com')->send(new AppointmentForm($details));
        }
        return view('success-booking');
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function myorder()
    {
        $order = BookOrder::with('service')->where('customer_id', Auth::user()->id)->get();
        return view('myorder', compact('order'));
    }

    public function myqoute()
    {
        $order = Quote::with('service')->where('customer_id', Auth::user()->id)->get();
        return view('myqoute', compact('order'));
    }
    
    public function myappointment()
    {
        $order = Appointment::with('service')->where('customer_id', Auth::user()->id)->get();
        return view('myappointment', compact('order'));
    }
    
    public function forgetpassword()
    {
        $user = User::where('id', Auth::user()->id)->get();
        return view('changepassword', compact('user'));
    }
    
    public function changepassword(Request $request)
    {
        $request->validate([
            'password'  => 'required'
        ]); 
        $user = User::where('id', Auth::user()->id)->update([
            'customerpassword'  => $request->password
        ]);
        return redirect(route('user-dashboard', app()->getLocale() ));
    }
}
