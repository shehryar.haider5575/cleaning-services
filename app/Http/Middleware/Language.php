<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next, string $lang)
    // {
    //     App::setlocale($lang);

    //     return $next($request);
    // }

    public function handle($request, Closure $next)
    {
        // dd($request->lang);
        \App::setLocale($request->lang);

        return $next($request);
    }
}