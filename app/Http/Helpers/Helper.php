<?php
use App\Models\City;
use App\Models\Page;
use App\Models\Slider;
use App\Models\States;
use App\Models\Country;
use App\Models\Product;
use App\Models\ContentPage;
use App\Models\SiteSetting;

/**
 * return array of resource
 * @return array
 */
function sliders()
{
    return Slider::whereNull('deleted_at')->where('status',1)->orderBy('created_at','desc')->take(3)->get();
}
/**
 * Get Specifeid Page Content
 */
function pageContent()
{
    $page_id = Page::where('name','LIKE','%Home%')->first();
    if(!empty($page_id))
    {
        return  ContentPage::whereNull('deleted_at')->where([['type',5],['status',1],['page_id',$page_id->id]])->first();
    }
}

/**
 * Get Specifeid Page Content
 */
function servicesProducts()
{
    return Product::whereNull('deleted_at')->where('status',1)->orderBy('created_at','desc')->select(['id','name'])->get();
}

/**
 * Get Specified Site Setting
 * @return array 
 */
function getConfig($key)
{
   $setting =  SiteSetting::where('key',$key)->first();
   if($setting)
   {
       return $setting->value;
   }
   return '';
}

function allcountry()
{
    return Country::all();
}

function status($id)
{
    return States::where('country_id', $id)->get();
}

function cities($id)
{
    return City::where('state_id', $id)->get();
}