<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class City extends Model
{
    protected $table    = 'cities';
    public $timestamps  = false;
}
