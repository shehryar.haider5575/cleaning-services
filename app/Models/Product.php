<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $fillable = ['name','featured_image','description','main_view','is_featured','status','created_by','updated_by'];

    /**
     * return an array of specified resource
     * @return array
     */
    public function productDetails()
    {
        return $this->hasMany('App\Models\ProductDetail', 'product_id', 'id');
    }


    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
   }
}
