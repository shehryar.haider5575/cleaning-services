<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class States extends Model
{
    protected $table    = 'states';
    public $timestamps  = false;
}
