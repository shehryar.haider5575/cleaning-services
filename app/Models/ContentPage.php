<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes; // <-- This is required

class ContentPage extends Model
{
    use SoftDeletes;
    protected $table = 'cms';
    protected $fillable = ['page_id','attachment','heading','sub_heading_1','sub_heading_2','text','type','status','created_by','updated_by'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
   }
}
