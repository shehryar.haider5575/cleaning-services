<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SiteSetting extends Model
{
    protected $table = 'site_settings';
    protected $fillable = ['key','value','created_by','updated_by'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
   }    
}
