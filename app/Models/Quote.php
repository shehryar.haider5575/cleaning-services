<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = 'get_quote';
    protected $fillable = ['service_id','customer_id','name','cnumber','ctime','email','comment','entry_date'];
    public $timestamps = false;

    public function service()
    {
        return $this->hasOne(ServiceDetail::class, 'id', 'service_id');
    }
}
