<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    protected $table = 'service';
    public $timestamps = false;

    /**
     * Get all of the serviceDetails for the Service
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function serviceDetails()
    {
        return $this->hasMany(ServiceDetail::class, 'categoryid', 'id');
    }
//     protected $fillable = ['attachment','name','description','status','created_by','updated_by'];

//     public static function boot()
//     {
//        parent::boot();
//        static::creating(function($model)
//        {
//            $user = Auth::user();
//            $model->created_by = $user->id;
//            $model->updated_by = $user->id;
//        });
//        static::updating(function($model)
//        {
//            $user = Auth::user();
//            $model->updated_by = $user->id;
//        });
//    }
}
