<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookOrder extends Model
{
    protected $table = 'book_order';
    protected $fillable = ['service_id','customer_id','vendor_id','address','work_time','heroes','instruction','rate','order_status','checkoutid','pay_orderid','pay_status','pay_amount','contact','entry_date'];
    public $timestamps = false;

    /**
     * Get the service associated with the BookOrder
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function service()
    {
        return $this->hasOne(ServiceDetail::class, 'id', 'service_id');
    }
}

