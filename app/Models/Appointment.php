<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $table = 'appointment';
    protected $fillable = ['service_id','app_id','customer_id','vendor_id','address','contact','adate','time','work_time','rate','heroes','instruction','entry_date'];
    public $timestamps = false;

    public function service()
    {
        return $this->hasOne(ServiceDetail::class, 'id', 'service_id');
    }
}
