<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDetail extends Model
{
    protected $table = 'servicecategory';
    protected $fillable = ['categoryid','servicecategory','service','slug','rate','unit','description','file','book','appointment','getquote','vat','entry_date'];
    public $timestamps = false;

    /**
     * Get the service associated with the ServiceDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pservice()
    {
        return $this->hasOne(Service::class, 'id', 'categoryid');
    }

    public function unitp()
    {
        return $this->hasOne(Unit::class, 'id', 'unit');
    }
}
